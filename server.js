const Express = require('express');
const compression = require('compression');
const http = require('http');
const https = require('https');
const fs = require('fs');
const Path = require('path');
const chalk  = require('chalk');
const httpProxy = require('http-proxy');
const appConf = require('./appConf.json');

const privateKey = fs.readFileSync('./cert/server.key', 'utf8');
const certificate = fs.readFileSync('./cert/server.crt', 'utf8');
const credentials = {key: privateKey, cert: certificate};
const SERVICE_URL = appConf.serviceUrl;
const proxyTarget = `https://${SERVICE_URL}/api`;

const proxy = httpProxy.createServer({
  target: proxyTarget,
  secure: false,
  cookieDomainRewrite: {
    [SERVICE_URL]: "localhost",
    "*": ""
  }
});

proxy.on('error', (error, req, res) => {
  let json;
  if (error.code !== 'ECONNRESET') {
    console.error('proxy error', error);
  }
  if (!res.headersSent) {
    res.writeHead(500, {'content-type': 'application/json'});
  }

  json = {error: 'proxy_error', reason: error.message};
  res.end(JSON.stringify(json));
});

const app = new Express();
const server = new https.createServer(credentials, app);
const serverPort = 5050;
const staticRoot = Path.join(__dirname,  './dist');

app.use(compression());
app.use(Express.static(staticRoot));
app.use('/api', (req, res) => {
  proxy.web(req, res)
});

app.use((req, res, next)=>{
  res.sendFile(Path.join(staticRoot, 'index.html'))
});

server.listen(serverPort, (err) => {
  if (err)
    return console.render(chalk.red(err));
  console.log(chalk.green('Server is running on https://localhost:' + serverPort), ', ', chalk.green('proxying to: ' + proxyTarget));
});


const httpApp = new Express();
const httpServer = new http.Server(httpApp);

httpApp.use(compression());
httpApp.use(Express.static(staticRoot));
httpApp.use('/api', (req, res) => {
  proxy.web(req, res)
});

httpApp.use((req, res, next)=>{
  res.sendFile(Path.join(staticRoot, 'index.html'))
});

httpServer.listen(6060, (err) => {
  if (err)
    return console.render(chalk.red(err));
  console.log(chalk.green('Server is running on http://localhost:' + 6060), ', ', chalk.green('proxying to: ' + proxyTarget));
});
