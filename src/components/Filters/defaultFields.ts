import * as moment from 'moment';
import {IFilterItemType} from './FilterItem';
import {IFiltersField} from './Filters';

const momentToday = moment();
const momentYesterday = moment().add(-1, 'days');
const lastDayRange = {
  dateFrom: momentYesterday,
  dateTo: momentToday,
  timeFrom: {
    hours: momentToday.hours(),
    minutes: momentToday.minutes()
  },
  timeTo: {
    hours: momentToday.hours(),
    minutes: momentToday.minutes()
  }
};

export const SuccessCalls: IFiltersField[] = [
  {
    id: 'dialTime',
    sqlId: 'invitedt',
    title: 'Время инициализации звонка',
    type: IFilterItemType.DATE_RANGE,
    value: lastDayRange,
  },
  {
    id: 'sideANumber',
    sqlId: 'anumber',
    title: 'Номер А',
    type: IFilterItemType.TEXT,
  },
  {
    id: 'sideBNumber',
    sqlId: 'bnumber',
    title: 'Номер Б',
    type: IFilterItemType.TEXT,
  },
  {
    id: 'duration',
    sqlId: 'duration',
    title: 'Продолжительность',
    type: IFilterItemType.NUMBER_RANGE,
  },
];

export const FailedCalls: IFiltersField[] = [
  {
    id: 'dialTime',
    sqlId: 'invitedt',
    title: 'Время инициализации звонка',
    type: IFilterItemType.DATE_RANGE,
    value: lastDayRange,
  },
  {
    id: 'sideANumber',
    sqlId: 'fromnumber',
    title: 'Номер А',
    type: IFilterItemType.TEXT,
  },
  {
    id: 'dialedNumber',
    sqlId: 'callednum',
    title: 'Набранный номер Б',
    type: IFilterItemType.TEXT,
  },
];
