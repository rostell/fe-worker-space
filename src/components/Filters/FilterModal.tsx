import * as React from 'react';
import cn from 'classnames';
import {
  Button,
  BTN_SECONDARY_TYPE,
  NumericInput,
  TextInput,
  MultiSelect,
  Datepicker,
  Timepicker,
  Toggle,
  IDatepickerRangeValue,
} from '@telsystems/inputs';
import {Align, Left, Right} from '@telsystems/design';
import {Hotkeys, CTRL_ENTER} from '@telsystems/common';
import {IFilterItemType} from './FilterItem';
import * as theme from './Filters.scss';

const DEFAULT_TRANSITION_DURATION = 500;

interface IFiterModalProps {
  title: string;
  value?: any;
  list?: string[];
  type: IFilterItemType;
  changeValue: (value?: any) => void;
  closeModal: (node, event: any) => void;
  style: any;
}

export class FilterModal extends React.Component<IFiterModalProps, any> {
  public constructor(props, context) {
    super(props, context);
    const {value, type} = props;
    this.state = {
      value,
      closed: false
    };
  }

  public componentWillReceiveProps(nextProps) {
    const {value, type} = nextProps;

    this.setState({
      value
    });
  }

  public render() {
    const {title, type, list, style} = this.props;
    const {value, closed} = this.state;

    return (
      <Hotkeys keyName={[CTRL_ENTER]} onKeyDown={this.onCloseModal(true)}>
        <div
          className={cn(
            theme['filter-modal'],
            closed ? theme['filter-modal-close'] : ''
          )}
          style={{
            ...style,
            animationDuration: `${DEFAULT_TRANSITION_DURATION}ms`
          }}
        >
          <h2 className={theme['filter-modal__header']}>{title}</h2>
          <div className={theme['filter-modal__inner']}>
            {this.renderValue(value, type, list)}
          </div>
          <Align className={theme['filter-modal__footer']}>
            <Right>
              <Button
                onClick={this.onCloseModal(false)}
                type={BTN_SECONDARY_TYPE}
              >
                Удалить
              </Button>
              <Button
                onClick={this.onCloseModal(true)}
                className={theme['filter-modal__apply-btn']}
              >
                Применить
              </Button>
            </Right>
          </Align>
        </div>
      </Hotkeys>
    );
  }

  private castType = (value: any, type: IFilterItemType) => {
    if (
      type === IFilterItemType.NUMBER ||
      type === IFilterItemType.NUMBER_RANGE
    ) {
      return parseInt(value, 10) || 0;
    }

    return value;
  }

  private renderValue = (value: any,
                         type: IFilterItemType,
                         list?: string[]) => {
    const updateValue = value =>
      this.setState({value: this.castType(value, type)});

    const updateTextListValue = ({title}) =>
      this.setState(({value}) => ({
        value: value
          ? value.includes(title)
            ? value.filter(item => item !== title)
            : [...value, title]
          : []
      }));

    const updateDateValue = (value: IDatepickerRangeValue) => {
      const {from, till} = value;
      this.setState(({value}) => ({
        value: {...value, dateFrom: from, dateTo: till}
      }));
    };

    const updateValueField = field => fieldValue => {
      this.setState(({value}) => ({
        value: {...value, [field]: this.castType(fieldValue, type)}
      }));
    };

    switch (type) {
      case IFilterItemType.NUMBER:
        return (
          <NumericInput value={value} onChange={updateValue} fullWidth={true} className={theme['filter-numeric-input']}/>
        );
      case IFilterItemType.TEXT:
      case IFilterItemType.TEXT_EQUAL:
        return (
          <TextInput value={value} onChange={updateValue} fullWidth={true}/>
        );
      case IFilterItemType.NUMBER_RANGE:
        return (
          <Align className={theme['inputs']}>
            <Left className={theme['inputs__item']}>
              <TextInput
                value={value && value.from}
                onChange={updateValueField('from')}
                fullWidth={true}
                placeholder="От"
              />
            </Left>
            <Right className={theme['inputs__item']}>
              <TextInput
                value={value && value.to}
                onChange={updateValueField('to')}
                fullWidth={true}
                placeholder="До"
              />
            </Right>
          </Align>
        );
      case IFilterItemType.TEXT_LIST:
        const items = list
          ? list.map(item => ({
            content: {title: item},
            selected: (value || []).includes(item)
          }))
          : [];
        return (
          <MultiSelect
            items={items}
            useCheckbox={true}
            fullWidth={true}
            onItemClick={updateTextListValue}
            clearSelectedButton={false}
            className={theme['filter-multiselect']}
          />
        );
      case IFilterItemType.DATE_RANGE:
        return (
          <div className={theme['daterange']}>
            <div className={theme['datepicker-wrapper']}>
              <Datepicker
                isRangeSelection={true}
                visibleMonthsCount={2}
                value={
                  value && value.dateFrom && value.dateTo && {
                    from: value.dateFrom,
                    till: value.dateTo
                  }
                }
                onChange={updateDateValue}
              />
            </div>
            <Align className={theme['timepicker-panel']}>
              <Left>Период инициализированного звонка</Left>
              <Right className={theme['timepicker-inputs']}>
                <div className={theme['timepicker-wrapper']}>
                  <Timepicker
                    value={value && value.timeFrom}
                    onValueChange={updateValueField('timeFrom')}
                    fullWidth={true}
                  />
                </div>
                —
                <div className={theme['timepicker-wrapper']}>
                  <Timepicker
                    value={value && value.timeTo}
                    onValueChange={updateValueField('timeTo')}
                    fullWidth={true}
                  />
                </div>
              </Right>
            </Align>
          </div>
        );
      case IFilterItemType.BOOLEAN:
        return (
          <Toggle checked={!!value} onChange={updateValue}/>
        );
      default:
        return <div>Ввод данных невозможен</div>;
    }
  }

  private onCloseModal = applyChanges => event => {
    const {changeValue, closeModal} = this.props;
    const {value} = this.state;

    this.setState({closed: true});
    setTimeout(() => {
      closeModal(null, event);
      if (applyChanges) {
        changeValue(value);
      } else {
        changeValue();
      }
    }, DEFAULT_TRANSITION_DURATION);
  }
}
