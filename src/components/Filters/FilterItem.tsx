import * as React from 'react';
import theme from './Filters.scss';
import cn from 'classnames';

export enum IFilterItemType {
  NUMBER = 'number',
  NUMBER_RANGE = 'number_range',
  TEXT = 'text',
  TEXT_EQUAL = 'text_equal',
  TEXT_LIST = 'text_list',
  DATE_RANGE = 'date_range',
  BOOLEAN = 'boolean',
}

interface IFiterItemProps {
  id: string;
  title: string;
  type: IFilterItemType;
  list?: string[];
  value?: any;
  onChange: (value: any) => void;
  onToggleModal: (node: any, event: any) => void;
  addItemTopPosition: (top: number) => void;
  isOpen: boolean;
}

export function valueToString(value: any, type: IFilterItemType): string {
  switch (type) {
    case IFilterItemType.NUMBER:
      return value.toString();
    case IFilterItemType.TEXT:
    case IFilterItemType.TEXT_EQUAL:
      return value;
    case IFilterItemType.NUMBER_RANGE:
      return `${value.from} — ${value.to}`;
    case IFilterItemType.TEXT_LIST:
      return value.join(', ');
    case IFilterItemType.DATE_RANGE:
      return `${value.dateFrom.format('DD.MM')} — \
${value.dateTo.format('DD.MM')}, \
${value.timeFrom.hours}:${value.timeFrom.minutes} — \
${value.timeTo.hours}:${value.timeTo.minutes}`;
    default:
      return JSON.stringify(value);
  }
}

export class FilterItem extends React.Component<IFiterItemProps, any> {
  private node: any;

  public constructor(props, context) {
    super(props, context);
  }

  public render() {
    const {
      id,
      title,
      type,
      value,
      list,
      onChange,
      onToggleModal,
      isOpen
    } = this.props;

    return (
      <div ref={this.getRef} className={theme['item-wrapper']}>
        {!value ? (
          <div className={cn(theme['item'])} onClick={this.onToggle}>
            {title}
          </div>
        ) : (
          <div
            className={cn(theme['item'], theme['active'])}
            onClick={this.onToggle}
          >
            {title} {valueToString(value, type)}
          </div>
        )}
      </div>
    );
  }

  private onToggle = (event: any): void => {
    this.props.onToggleModal(this.node, event);
  }

  private getRef = (node: any): void => {
    this.node = node;

    const {top = 0} = (node && node.getBoundingClientRect()) || {};
    const {addItemTopPosition} = this.props;

    addItemTopPosition(top);
  }
}
