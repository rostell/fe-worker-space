import * as React from 'react';
import * as moment from 'moment';
import { IFilterItemType } from '../index';
import { valueToString } from '../FilterItem';

describe('valueToString', () => {
  it('should form condition with number', () => {
    const field = {
      id: 'fieldId',
      sqlId: 'fieldSqlId',
      title: 'fieldTitle',
      type: IFilterItemType.NUMBER,
      value: 123
    };

    expect(valueToString(field.value, field.type)).toEqual('123');
  });

  it('should form condition with string', () => {
    const field = {
      id: 'fieldId',
      sqlId: 'fieldSqlId',
      title: 'fieldTitle',
      type: IFilterItemType.TEXT,
      value: 'some text'
    };

    expect(valueToString(field.value, field.type)).toEqual('some text');
  });

  it('should form condition with string list', () => {
    const field = {
      id: 'fieldId',
      sqlId: 'fieldSqlId',
      title: 'fieldTitle',
      type: IFilterItemType.TEXT_LIST,
      value: ['first', 'second', 'third']
    };

    expect(valueToString(field.value, field.type)).toEqual(
      'first, second, third'
    );
  });

  it('should form condition with number range', () => {
    const field = {
      id: 'fieldId',
      sqlId: 'fieldSqlId',
      title: 'fieldTitle',
      type: IFilterItemType.NUMBER_RANGE,
      value: { from: 100, to: 200 }
    };

    expect(valueToString(field.value, field.type)).toEqual('100 — 200');
  });

  it('should form condition with date range', () => {
    const field = {
      id: 'fieldId',
      sqlId: 'fieldSqlId',
      title: 'fieldTitle',
      type: IFilterItemType.DATE_RANGE,
      value: {
        dateFrom: moment('01/12/2016', 'DD/MM/YYYY', true),
        dateTo: moment('10/03/2017', 'DD/MM/YYYY', true),
        timeFrom: {
          hours: 10,
          minutes: 25
        },
        timeTo: {
          hours: 20,
          minutes: 35
        }
      }
    };

    expect(valueToString(field.value, field.type)).toEqual(
      '01.12 — 10.03, 10:25 — 20:35'
    );
  });
});
