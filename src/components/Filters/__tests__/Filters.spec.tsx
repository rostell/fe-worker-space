import * as React from 'react';
import {shallow, mount} from 'enzyme';
import {Filters} from '../index';
import {IFilterItemType} from '../FilterItem';

describe('Filters', () => {
  describe('render', () => {
    const fields = [
      {
        id: 'fieldId',
        list: ['fieldList'],
        sqlId: 'fieldSqlId',
        title: 'fieldTitle',
        type: IFilterItemType.TEXT,
        value: 123
      }
    ];
    const component = shallow(
      <Filters fields={fields} onChangeValue={() => {}}/>
    );
    it('should render FilterItem', () => {
      expect(component.find('FilterItem').length).toEqual(1);
    });
    it('FilterItem should have correctly props', () => {
      expect(component.find('FilterItem').props().id).toEqual(fields[0].id);
      expect(component.find('FilterItem').props().title).toEqual(fields[0].title);
      expect(component.find('FilterItem').props().type).toEqual(fields[0].type);
    });
  });
});
