import * as React from 'react';
import {mount} from 'enzyme';
import {IFilterItemType, FilterModal} from '../index';

describe('FilterItem', () => {
  describe('render', () => {
    it('should render Datepicker if type date_range', () => {
      const component = mount(
        <FilterModal title={'modalTitle'} type={IFilterItemType.DATE_RANGE} changeValue={() => {}} closeModal={() => {}} style={null}/>
      );
      expect(component.find('.datepicker-wrapper').length).toEqual(1);
    });
    it('should render multiselect if type text_list', () => {
      const component = mount(
        <FilterModal title={'modalTitle'} type={IFilterItemType.TEXT_LIST} changeValue={() => {}} closeModal={() => {}} style={null}/>
      );
      expect(component.find('.filter-multiselect').length).toBeGreaterThan(1);
    });
    it('should render two TextInputs if type number_range', () => {
      const component = mount(
        <FilterModal title={'modalTitle'} type={IFilterItemType.NUMBER_RANGE} changeValue={() => {}} closeModal={() => {}} style={null}/>
      );
      expect(component.find('input').length).toEqual(2);
    });
    it('should render NumericInput if type number', () => {
      const component = mount(
        <FilterModal title={'modalTitle'} type={IFilterItemType.NUMBER} changeValue={() => {}} closeModal={() => {}} style={null}/>
      );
      expect(component.find('.filter-numeric-input').length).toBeGreaterThan(1);
    });
    it('should render TextInput if type text', () => {
      const component = mount(
        <FilterModal title={'modalTitle'} type={IFilterItemType.TEXT} changeValue={() => {}} closeModal={() => {}} style={null}/>
      );
      expect(component.find('input').length).toEqual(1);
    });
  });
});
