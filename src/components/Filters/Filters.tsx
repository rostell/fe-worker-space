import * as React from 'react';
import cn from 'classnames';
import theme from './Filters.scss';
import { Align, Left, Right } from '@telsystems/design';
import { FilterItem, IFilterItemType } from './FilterItem';
import { FilterModal } from './FilterModal';
import {Hotkeys, ESC} from '@telsystems/common';

export interface IFiltersField {
  id: string;
  title: string;
  type: IFilterItemType;
  value?: any;
  list?: string[];
  sqlId?: string;
}

interface IFiltersProps {
  fields: IFiltersField[];
  onChangeValue: (value: any) => void;
  className?: string;
}

export const DEFAULT_FILTER_TRANSITION_DURATION = 300;
export const FILTER_ITEM_HEIGHT = 38;
export const MODAL_MAX_WIDTH = 400;

export class Filters extends React.Component<IFiltersProps, any> {
  private node: any;

  public constructor(props, context) {
    super(props, context);

    this.state = {
      showAll: false,
      openIndex: -1,
      showButton: false,
      filtersHeight: FILTER_ITEM_HEIGHT,
      itemsTopPosition: []
    };
  }

  public render() {
    const { fields, className } = this.props;
    const {
      showAll,
      openIndex,
      modalPosition,
      swowButton,
      filtersHeight,
      itemsTopPosition
    } = this.state;

    const shownActiveCount =
      itemsTopPosition.length > 0
        ? itemsTopPosition.filter(
            (item, index) => item === itemsTopPosition[0] && fields[index] && fields[index].value
          ).length
        : 0;
    const hiddenActiveCount =
      fields.filter(field => field.value).length - shownActiveCount;

    return (
      <div ref={this.getRef} className={cn(theme['wrapper'], className)}>
        <Align>
          <Left className={theme['left']}>
            <div
              className={theme['filters-wrapper']}
              style={{
                maxHeight: `${showAll ? filtersHeight : FILTER_ITEM_HEIGHT}px`,
                transitionDuration: `${DEFAULT_FILTER_TRANSITION_DURATION}ms`
              }}
            >
              <div ref={this.getFilterRef} className={theme['filters']}>
                {this.renderFilters(fields)}
              </div>
            </div>
          </Left>
          <Right>
            {swowButton &&
              (!showAll ? (
                <button
                  onClick={this.onToggleShowAll}
                  type="button"
                  className={cn(theme['item'], theme['more-btn'])}
                >
                  Ещё
                  {hiddenActiveCount > 0 && (
                    <span>&nbsp;&middot;&nbsp;{hiddenActiveCount}</span>
                  )}
                </button>
              ) : (
                <button
                  onClick={this.onToggleShowAll}
                  type="button"
                  className={cn(theme['item'], theme['hide-btn'])}
                >
                  Скрыть
                </button>
              ))}
          </Right>
        </Align>
        {openIndex !== -1 &&
          this.renderModal({
            ...fields[openIndex],
            ...modalPosition,
            changeValue: this.onChange(fields[openIndex].id),
            closeModal: this.onToggleModal(openIndex)
          })}
      </div>
    );
  }

  private renderFilters = (filters: IFiltersField[]): React.ReactNode[] => {
    const { openIndex } = this.state;

    return filters.map((field, index) => (
      <FilterItem
        {...field}
        onToggleModal={this.onToggleModal(index)}
        isOpen={openIndex === index}
        onChange={this.onChange(field.id)}
        key={field.id}
        addItemTopPosition={this.addItemTopPosition}
      />
    ));
  }

  private renderModal = ({
    title,
    value,
    list,
    type,
    changeValue,
    closeModal,
    top,
    left
  }) => (
    <Hotkeys keyName={[ESC]} onKeyDown={this.onFilterModalHotkey}>
      <FilterModal
        title={title}
        value={value}
        list={list}
        type={type}
        changeValue={changeValue}
        closeModal={closeModal}
        style={{ top, left }}
      />
    </Hotkeys>
  )

  private onFilterModalHotkey = (key: string): void => {
    if (key === ESC) {
      document.removeEventListener('click', this.onCloseModal);
      this.setState({ openIndex: -1 });
    }
  }

  private getRef = (node: any): void => {
    this.node = node;
  }

  private getFilterRef = (node: any): void => {
    const filtersHeight = node ? node.getBoundingClientRect().height : 0;

    if (filtersHeight > FILTER_ITEM_HEIGHT) {
      this.setState({ swowButton: true, filtersHeight });
    }
  }

  private addItemTopPosition = (top: number) => {
    this.setState(({ itemsTopPosition }) => ({
      itemsTopPosition: [...itemsTopPosition, top]
    }));
  }

  private onToggleShowAll = event => {
    this.setState(({ showAll }) => ({ showAll: !showAll, openIndex: -1 }));
    document.removeEventListener('click', this.onCloseModal);
  }

  private onCloseModal = event => {
    if (this.node && !this.node.contains(event.target)) {
      document.removeEventListener('click', this.onCloseModal);
      this.setState({ openIndex: -1 });
    }
  }

  private onToggleModal = (index: number) => (node: any, event) => {
    const { openIndex } = this.state;

    if (index !== openIndex) {
      document.addEventListener('click', this.onCloseModal);
    }

    const { left: parentX = 0, top: parentY = 0, width: parentWidth = 0 } =
      (this.node && this.node.getBoundingClientRect()) || {};
    const { left: nodeX = 0, top: nodeY = 0 } =
      (node && node.getBoundingClientRect()) || {};

    this.setState({
      openIndex: index === openIndex ? -1 : index,
      modalPosition: { left: Math.min(parentWidth - MODAL_MAX_WIDTH, nodeX - parentX), top: nodeY - parentY }
    });
  }

  private onChange = (fieldId: string) => (
    updatedFieldValue: IFiltersField
  ) => {
    const { fields, onChangeValue } = this.props;
    const updatedFieldId = fields.findIndex(field => field.id === fieldId);
    const updatedField = { ...fields[updatedFieldId], value: updatedFieldValue};
    const updatedFields =  [...fields.filter((field, index) => index !== updatedFieldId && field.value), updatedField];

    if (updatedField) {
     onChangeValue(updatedFields);
    }
  }
}
