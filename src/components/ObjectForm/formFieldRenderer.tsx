import * as React from 'react';
import {TextInput, Textarea, Select, NumericInput, ISelectItem} from '@telsystems/inputs';
import style from './formFieldRenderer.scss';
import {AudioPlayer} from '@telsystems/player';

export function textInput(value: any, readonly: boolean, onChange: (value) => void): JSX.Element {
  const disabledClass = readonly ? style['disabled'] : undefined;
  return (
    <TextInput
      fullWidth={true}
      placeholder={'Введите значение'}
      className={disabledClass}
      onChange={onChange}
      disabled={readonly}
      value={value}
    />
  );
}

export function numericInput(value: any, readonly: boolean, onChange: (value) => void): JSX.Element {
  const disabledClass = readonly ? style['disabled'] : undefined;
  return (
    <NumericInput
      fullWidth={true}
      placeholder={'Введите значение'}
      className={disabledClass}
      onChange={onChange}
      disabled={readonly}
      value={value}
    />
  );
}

export function textarea(value: any, readonly: boolean, onChange: (value) => void): JSX.Element {
  const disabledClass = readonly ? style['disabled'] : undefined;
  return (
    <Textarea
      fullWidth={true}
      disabled={readonly}
      placeholder={'Введите значение'}
      onChange={onChange}
      className={disabledClass}
      value={value}
      autosize={{minRows: 3}}
    />
  );
}

export function select(value: any, readonly: boolean, onChange: (value) => void, items: ISelectItem[]): JSX.Element {
  return (
    <Select
      fullWidth={true}
      onItemClick={onSelectItem(onChange)}
      items={items}
      disabled={readonly}
      placeholder={'Выберите значение'}
      selectedIndex={getSelectedIndexByValue(value, items)}
    />
  );
}

export function player(src: string, onFail: () => void, isFailed: boolean): JSX.Element {
  return src && !isFailed
    ? <AudioPlayer src={src} onError={onFail} fullWidth={true}/>
    : <div className={style['error']}>Запись не загружена</div>;
}

function getSelectedIndexByValue(value: any, items): number | undefined {
  const foundIndex = items.findIndex(i => i.content.value === value);
  return foundIndex > -1 ? foundIndex : undefined;
}

function onSelectItem(callback) {
  return function(itemData) {
    callback(itemData.value);
  };
}
