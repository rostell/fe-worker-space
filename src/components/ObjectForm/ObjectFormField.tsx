import * as React from 'react';
import cn from 'classnames';
import style from './ObjectForm.scss';
import {IDataSchemeItem, ISelectMetadataItem} from './ObjectForm';
import {Label} from '@telsystems/design';
import * as fieldRenderer from './formFieldRenderer';
import {ISeparatedFieldData} from './ObjectFormWindow';
import {ISelectItem} from '@telsystems/inputs';

export enum EFieldType {
  TEXT_INPUT = 'text_input',
  TEXTAREA = 'textarea',
  TOGGLE = 'toggle',
  MULTISELECT = 'multiselect',
  SELECT = 'select',
  NUMBER_INPUT = 'number_input',
  PLAYER = 'player',
  CUSTOM = 'custom',
}

interface IObjectFormFieldProps {
  schemeData: IDataSchemeItem;
  value: any;
  onChange: (value: any) => void;
  readonly: boolean;
  addSeparatedFieldData?: (data: ISeparatedFieldData) => void;
  renderCustomField?: (schemeData: IDataSchemeItem, value: any, onChange) => JSX.Element;
}

interface IState {
  isFailed: boolean;
}

export class ObjectFormField extends React.Component <IObjectFormFieldProps, IState> {
  private fieldRef: HTMLElement | null;

  public constructor(props, context) {
    super(props, context);
    this.state = {
      isFailed: false
    };
  }

  public componentDidMount() {
    const {schemeData, addSeparatedFieldData} = this.props;
    if (schemeData.separatorTitle && this.fieldRef && addSeparatedFieldData) {
      const params: ISeparatedFieldData = {
        id: schemeData.id,
        offsetTop: this.fieldRef.offsetTop
      };
      addSeparatedFieldData(params);
    }
  }

  public render() {
    const {isFailed} = this.state;
    const {schemeData} = this.props;
    return (
      <>
        {
          schemeData.separatorTitle
            ? <div ref={this.getRef} className={style['separator']}>{schemeData.separatorTitle}</div>
            : null
        }
        {
          !isFailed
            ? (
              <Label
                className={style['label']}
                text={schemeData.title}
              >
                {this.renderField(schemeData)}
              </Label>
            )
            : <div className={style['error']}>Запись не загружена</div>
        }
      </>
    );
  }

  private getRef = (node): void => {
    this.fieldRef = node;
  }

  private renderField = (field: IDataSchemeItem): JSX.Element | null => {
    const {value, readonly, onChange, schemeData, renderCustomField} = this.props;
    const disabled = !!(readonly || field.disabled);
    switch (field.field) {
      case EFieldType.TEXTAREA:
        return fieldRenderer.textarea(value, disabled, onChange);
      case EFieldType.SELECT:
        const formattedItems = schemeData.items ? formatSelectItems(schemeData.items) : [];
        return fieldRenderer.select(value, disabled, onChange, formattedItems);
      case EFieldType.CUSTOM:
        return renderCustomField ? renderCustomField(schemeData, value, onChange) : null;
      case EFieldType.PLAYER:
        return fieldRenderer.player(value, this.onFail, this.state.isFailed);
      case EFieldType.NUMBER_INPUT:
        return fieldRenderer.numericInput(value, disabled, onChange);
      case EFieldType.TEXT_INPUT:
      default:
        return fieldRenderer.textInput(value, disabled, onChange);
    }
  }

  private onFail = (): void => {
    this.setState({
      isFailed: true
    });
  }
}

function formatSelectItems(items: ISelectMetadataItem[]): ISelectItem[] {
  return items.map(({title, value}) => ({content: {title, value}}));
}
