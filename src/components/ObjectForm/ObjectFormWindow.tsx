import * as React from 'react';
import cn from 'classnames';
import Scrollbars from 'react-custom-scrollbars';
import {BasicFormWindow, IBasicFormWindowProps} from '../../classes/BasicFormWindow';
import {IObjectFormProps, ObjectForm} from './ObjectForm';
import style from './ObjectForm.scss';

interface IObjectFormWindowProps extends IBasicFormWindowProps {
}

type TObjectFormWindowProps = IObjectFormWindowProps & IObjectFormProps;

export interface ISeparatedFieldData {
  id: string;
  offsetTop: number;
}

interface IObjectFormState {
  sidebarActiveItemId: string | null;
  isSidebarScrollingByClick: boolean;
}

const SCROLL_ANIMATION_STEP = 10;
const SCROLL_ANIMATION_DURATION = 300;

export class ObjectFormWindow extends BasicFormWindow<TObjectFormWindowProps, IObjectFormState> {
  private scrollbarRef;
  private separatedFieldsData: ISeparatedFieldData[] = [];

  constructor(props, context) {
    super(props, context);
    this.state = {
      sidebarActiveItemId: null,
      isSidebarScrollingByClick: false,
    };
  }

  protected renderFormComponent = (): JSX.Element => {
    const {onClose, title, show, footerElements, shouldFormClose, children, ...rest} = this.props;
    return (
      <div className={style['scrollbar']}>
        <Scrollbars
          onScrollFrame={this.onScroll}
          ref={this.getScrollbarRef}
          autoHide={true}
          className={style['scrollbar-component']}
        >
          <ObjectForm addSeparatedFieldData={this.addSeparatedFieldData} {...rest}/>
        </Scrollbars>
      </div>
    );
  }

  protected renderSidebar = (): JSX.Element | JSX.Element[] | null => {
    const {scheme} = this.props;
    const {sidebarActiveItemId} = this.state;
    const items = scheme.filter(s => s.separatorTitle);
    if (items.length) {
      return (
        <div className={style['sidebar-content']}>
          {
            items.map((item, index) => {
              const isActive = (sidebarActiveItemId === item.id) || (!sidebarActiveItemId && index === 0);
              const activeClass = isActive ? style['sidebar-item-active'] : null;
              return (
                <div
                  key={index}
                  onClick={this.scrollToSeparator(item.id)}
                  className={cn(style['sidebar-item'], activeClass)}
                >
                  {item.separatorTitle}
                </div>
              );
            })
          }
        </div>
      );
    }
    return null;
  }

  private getScrollbarRef = (node): void => {
    this.scrollbarRef = node;
  }

  private setSidebarItemId = (id: string): void => {
    if (this.state.sidebarActiveItemId !== id) {
      this.setState({
        sidebarActiveItemId: id
      });
    }
  }

  private addSeparatedFieldData = (data: ISeparatedFieldData): void => {
    this.separatedFieldsData.push(data);
  }

  private onScroll = (values) => {
    const {isSidebarScrollingByClick} = this.state;
    const separatedField = this.separatedFieldsData
      .sort((a, b) => b.offsetTop - a.offsetTop)
      .find(item => values.scrollTop >= item.offsetTop);
    if (!isSidebarScrollingByClick && separatedField) {
      const {id} = separatedField;
      this.setSidebarItemId(id);
    }
  }

  private scrollToSeparator = (id: string) => (): void => {
    const item = this.separatedFieldsData.find(item => item.id === id);
    if (item) {
      this.setSidebarItemId(id);
      this.setState({
        isSidebarScrollingByClick: true
      });
      const {offsetTop} = item;
      let scrollValue = this.scrollbarRef.getScrollTop();
      const step = offsetTop >= scrollValue ? SCROLL_ANIMATION_STEP : -SCROLL_ANIMATION_STEP;
      const animationInterval = SCROLL_ANIMATION_DURATION / Math.abs(offsetTop - scrollValue) * SCROLL_ANIMATION_STEP;
      const smooth = setInterval(() => {
        scrollValue += step;
        if (step > 0 && scrollValue > offsetTop || step < 0 && scrollValue < offsetTop) {
          scrollValue = offsetTop;
          clearInterval(smooth);
          this.setState({
            isSidebarScrollingByClick: false,
          });
        }
        this.scrollbarRef.scrollTop(scrollValue);
      }, animationInterval);
    }
  }
}
