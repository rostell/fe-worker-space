import * as React from 'react';
import style from './ObjectForm.scss';
import {ISchemeItem, ITableRecord} from '@telsystems/table';
import {EFieldType, ObjectFormField} from './ObjectFormField';
import {ISeparatedFieldData} from './ObjectFormWindow';
import {Record} from '@telsystems/helpers';

export interface ISelectMetadataItem {
  title: string;
  value: string;
}

export interface IDataSchemeItem extends ISchemeItem {
  field?: EFieldType;
  disabled?: boolean;
  items?: ISelectMetadataItem[];
  separatorTitle?: string;
}

export interface IObjectFormProps {
  formData: ITableRecord;
  readonly: boolean;
  onChange: (field: string, value: any) => void;
  scheme: IDataSchemeItem[];
  addSeparatedFieldData?: (data: ISeparatedFieldData) => void;
  renderCustomField?: (schemeData: IDataSchemeItem, value: any, onChange) => JSX.Element;
}

interface IState {
}

export class ObjectForm extends React.Component <IObjectFormProps, IState> {
  public render() {
    const {scheme, formData, readonly, addSeparatedFieldData, renderCustomField} = this.props;
    return (
      <div className={style['form-content']}>
        {
          scheme.map((item) => {
            const value = new Record(formData).get(item.id);
            return item.id === 'id' && !value
              ? null
              : (
                <ObjectFormField
                  schemeData={item}
                  renderCustomField={renderCustomField}
                  onChange={this.onChange(item.id)}
                  value={value}
                  addSeparatedFieldData={addSeparatedFieldData}
                  readonly={readonly}
                />
              );
          })
        }
      </div>
    );
  }

  private onChange = (field: string) => (value: any): void => {
    this.props.onChange(field, value);
  }
}
