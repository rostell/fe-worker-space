import * as React from 'react';
import cn from 'classnames';
import style from './EmptyPagePlaceholder.scss';
import {Icon} from '@telsystems/design';

interface IProps {
  title?: string;
  className?: string;
  children?: string | JSX.Element | JSX.Element[];
}

interface IState {
}

export class EmptyPagePlaceholder extends React.PureComponent <IProps, IState> {
  public render() {
    const {title, className, children} = this.props;
    return (
      <div className={cn(style['empty-page-placeholder'], className)}>
        <div className={style['box']}>
          <div className={style['text']}>{title}{children}</div>
        </div>
      </div>
    );
  }
}
