import * as React from 'react';
import {connect} from 'react-redux';
import {MBasePage} from './MBasePage';
import {SnackBar} from '@telsystems/modals';
import style from './BasePage.scss';
import {CBasePage} from './CBasePage';
import {Authorize, Page} from '@telsystems/common';
import {EFieldType, EmptyPagePlaceholder, IDataSchemeItem, LoaderOverlay, ObjectFormWindow} from '../index';
import {ITableRecord, Table} from '@telsystems/table';
import {Pagination} from '@telsystems/inputs';
import {TTableCell} from '../../../node_modules/@telsystems/table/dist/TTableCell';

interface IInternalProps {
}

interface IState {
}

export class PBasePage<P extends IInternalProps = {}, S extends IState = {}> extends React.Component<P, S> {
  public controller: CBasePage<MBasePage>;
  public formTitle = 'объекта';

  public constructor(props, context) {
    super(props, context);
  }

  public componentDidMount() {
    this.controller.init();
  }

  public render() {
    const records = this.controller.getPreparedRecords();
    const scheme = this.controller.getScheme();
    const isWaiting = this.controller.isWaiting();
    const error = this.controller.getError();
    const formData = this.controller.getFormData();
    return (
      <Authorize needLogin={true}>
        <Page className={style['page']}>
          {
            this.renderToolbar()
          }
          {
            records && records.length && !error
              ? (
                <Table
                  scheme={this.controller.getTableScheme()}
                  records={records}
                  renderCellData={this.renderCells}
                  footerElements={this.renderTableFooter()}
                  onDoubleClick={this.controller.onTableDoubleClick}
                  showPlaceholder={isWaiting}
                />
              )
              : <EmptyPagePlaceholder title={error || 'Не найдено ни одной записи'}/>
          }
          {
            !!formData
              ? this.renderForm(formData, scheme)
              : null
          }
          <LoaderOverlay isActive={isWaiting}/>
        </Page>
      </Authorize>
    );
  }

  protected renderToolbar = (): JSX.Element => {
    return <div/>;
  }

  protected renderForm = (formData: ITableRecord, scheme: IDataSchemeItem[]): JSX.Element => {
    const isReadOnly = this.controller.isReadOnly();
    const title = isReadOnly
      ? `Просмотр ${this.formTitle}` : (formData.id ? `Редактирование ${this.formTitle}` : `Создание ${this.formTitle}`);
    const formScheme = this.controller.getFormScheme();
    return (
      <ObjectFormWindow
        scheme={formScheme.length ? formScheme : scheme}
        readonly={isReadOnly}
        title={title}
        onClose={this.controller.resetFormData}
        show={true}
        error={this.controller.getFormError()}
        isWaiting={this.controller.isFormWaiting()}
        renderCustomField={this.renderCustomField}
        onChange={this.controller.onFormDataChange}
        footerElements={this.getFooterElements()}
        formData={formData}
      />
    );
  }

  protected renderCustomField = (schemeData: IDataSchemeItem, value: any, onChange): JSX.Element => {
    return <div/>;
  }

  protected getFooterElements = (): JSX.Element[] | JSX.Element => {
    return [];
  }

  protected renderTableFooter = (): JSX.Element | null => {
    const lastPage = this.controller.getLastPage();
    return lastPage > 1
      ? (
        <Pagination
          activePage={this.controller.getActivePage()}
          createLink={this.controller.createPageLinks}
          pagesCount={lastPage}
        />
      )
      : null;
  }

  private renderCells = (cellParams, record: ITableRecord): TTableCell => {
    if (cellParams.field === EFieldType.CUSTOM) {
      return undefined;
    }
    return record[cellParams.id];
  }
}
