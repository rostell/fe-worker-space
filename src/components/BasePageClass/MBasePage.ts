import {ViewModel} from '@telsystems/common';
import {ISchemeItem, ITableRecord} from '@telsystems/table';
import {IDataSchemeItem} from '../ObjectForm';
import {IResponse} from '../../declarations/main';

export interface IStateBasePage {
  isWaiting: boolean;
  error: string | null;
  scheme: IDataSchemeItem[];
  records: ITableRecord[];
  recordsCount: number;
  formError: string | null;
  isFormWaiting: boolean;
  isReadOnly: boolean;
  formData: ITableRecord | null;
  formScheme: ISchemeItem[];
}

export class MBasePage extends ViewModel<IStateBasePage> {
  public static defaults: IStateBasePage = {
    isWaiting: false,
    error: null,
    recordsCount: 0,
    formError: null,
    isFormWaiting: false,
    formScheme: [],
    isReadOnly: true,
    scheme: [],
    records: [],
    formData: null
  };

  public updateState(updates: any): IStateBasePage {
    return this.update(updates).getState();
  }

  public isFormWaiting = (): boolean => this.state.isFormWaiting;

  public getFormError = (): string | null => this.state.formError;

  public getRecordsCount = (): number => this.state.recordsCount;

  public getFormData = (): ITableRecord | null => this.state.formData;

  public isReadOnly = (): boolean => this.state.isReadOnly;

  public getScheme = (): IDataSchemeItem[] => this.state.scheme;

  public getFormScheme = (): ISchemeItem[] => this.state.formScheme;

  public getRecords = (): ITableRecord[] => this.state.records;

  public isWaiting = (): boolean => this.state.isWaiting;

  public getError = (): string | null => this.state.error;

  public onWaiting(): IStateBasePage {
    return this.updateState({isWaiting: true, error: null});
  }

  public onFail(error: string): IStateBasePage {
    return this.updateState({isWaiting: false, error});
  }

  public onDataLoadSuccess(resp: IResponse): IStateBasePage {
    if (resp.resultcode !== 0) {
      return this.onFail(resp.resultmsg);
    }
    const isCountLoaded = resp.data && typeof resp.data.count === 'number';
    const records = isCountLoaded ? this.state.records : resp.data;
    const recordsCount = isCountLoaded ? resp.data.count : this.state.recordsCount;
    return this.updateState({isWaiting: false, error: null, records, recordsCount});
  }

  public onFormFail(formError: string): IStateBasePage {
    return this.updateState({isFormWaiting: false, formError});
  }

  public onFormWaiting(): IStateBasePage {
    return this.updateState({isFormWaiting: true, formError: null});
  }

  public onFormSaveSuccess(resp: IResponse): IStateBasePage {
    if (resp.resultcode !== 0) {
      return this.onFormFail(resp.resultmsg);
    }
    return this.updateState({isFormWaiting: false, formError: null});
  }
}
