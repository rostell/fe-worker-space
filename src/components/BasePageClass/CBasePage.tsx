import {MBasePage} from './MBasePage';
import {BasicController} from '@telsystems/common';
import {IEventResult, ISchemeItem, ITableRecord} from '@telsystems/table';
import {MODAL_ANIMATION_DURATION} from '@telsystems/modals';
import {Record} from '@telsystems/helpers';
import {PATHS} from '../../routes/paths';
import {EFieldType, IDataSchemeItem} from '../ObjectForm';

export class CBasePage<T extends MBasePage> extends BasicController<T> {
  public model: T;
  public pagePath = '';
  public RECORDS_BY_PAGE = 20;

  public componentWillReceiveProps(currentProps, nextProps) {
    super.componentWillReceiveProps(currentProps, nextProps);
    if (currentProps.match.params.page !== nextProps.match.params.page) {
      this.loadRecords();
    }
  }

  public init(): void {
    this.loadRecords();
  }

  public loadRecords(): void {
  }

  public onFormDataChange = (field: string, value: any): void => {
  }

  public getFormData = (): ITableRecord | null => this.model.getFormData();

  public onTableDoubleClick = (event: IEventResult<ITableRecord>): void => {
  }

  public resetFormData = (): void => {
  }

  public isReadOnly = (): boolean => this.model.isReadOnly();

  public getPreparedRecords = (): ITableRecord[] => {
    return this.model.getRecords().map(item => this.normalizeData(item, this.getScheme()));
  }

  public getScheme = (): IDataSchemeItem[] => this.model.getScheme();

  public getFormError = (): string | null => this.model.getFormError();

  public getTableScheme = (): IDataSchemeItem[] => {
    return this.getScheme().filter(s => s.field !== EFieldType.CUSTOM);
  }

  public isWaiting(): boolean {
    return this.model.isWaiting();
  }

  public createPageLinks = (pageNum: number): string => {
    return `${PATHS.INDEX}/${this.pagePath}/page/${pageNum}`;
  }

  public getActivePage = (): number => {
    const {page} = this.nextProps.match.params;
    return page ? parseInt(page, 10) : 1;
  }

  public getLastPage(): number {
    const count = this.model.getRecordsCount();
    return Math.ceil(count / this.RECORDS_BY_PAGE);
  }

  public getFormScheme = () => this.model.getFormScheme();

  public isFormWaiting(): boolean {
    return this.model.isFormWaiting();
  }

  public getError(): string | null {
    return this.model.getError();
  }

  public normalizeData(data: ITableRecord, scheme: ISchemeItem[]) {
    const recordData = new Record(data);
    return scheme.reduce((nData, schemeItem): ITableRecord => {
      nData[schemeItem.id] = recordData.get(schemeItem.id);
      return nData;
    }, {});
  }
}
