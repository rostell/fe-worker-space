import * as React from 'react';
import {Overlay, SvgLoader} from '@telsystems/design';
import style from './LoaderOverlay.scss';

export interface ILoaderOverlay {
  isActive?: boolean;
}
interface IState {}

export class LoaderOverlay extends React.PureComponent <ILoaderOverlay, IState> {
  public static defaultProps: ILoaderOverlay = {
    isActive: false,
  };

  public render() {
    const {isActive} = this.props;
    return isActive
      ?
      (
        <Overlay className={style['loader-overlay']} isLight={true}>
          <SvgLoader className={style['loader-icon']}/>
        </Overlay>
      )
      : null;
  }
}
