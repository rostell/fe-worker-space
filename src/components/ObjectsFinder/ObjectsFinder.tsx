import * as React from 'react';
import * as MVC from 'react-redux-mvc';
import {ISelectItem, SuggestingInput} from '@telsystems/inputs';
import {CObjectsFinder, formatQueryToValue} from './CObjectsFinder';
import {Icon, Tooltip, TOOLTIP_POSITION, TOOLTIP_SIZE} from '@telsystems/design';
import {ViewModel} from '@telsystems/common';

export interface IFieldsName {
  name?: string;
  title?: string;
  id: string;
}

const ENTER_KEY = 13;
export type TOnFilterChangeCallback = (value: string, isNewUrl?: boolean) => void;

interface IProps {
  // временный костыль, т.к. поиска нет, есть только фильтр
  onFilterChange: TOnFilterChangeCallback;
  isWaiting?: boolean;
  className?: string;
  fields: IFieldsName[];
}

interface IState {
  value: string;
  items: ISelectItem[];
  activeTooltip: boolean;
  isFiltered: boolean;
  isFocused: boolean;
}

@MVC.withController(CObjectsFinder)
export class ObjectsFinder extends React.PureComponent <IProps, IState> {
  private inputField: HTMLInputElement | null;
  private suggestingInput: HTMLElement | null;

  private controller: CObjectsFinder<ViewModel<any>>;

  public constructor(props, context) {
    super(props, context);
    this.state = {
      value: '',
      items: [],
      activeTooltip: false,
      isFiltered: false,
      isFocused: false
    };
  }

  public componentDidMount() {
    this.setState({
      items: this.controller.formatSuggestItems(this.props.fields),
      value: formatQueryToValue()
    });
    document.addEventListener('click', this.onOutSideClick);
    window.addEventListener('popstate', this.onPopState);
  }

  public componentWillUnmount() {
    document.removeEventListener('click', this.onOutSideClick);
    window.removeEventListener('popstate', this.onPopState);
  }

  public componentWillReceiveProps(nextProps) {
    this.setState(prevState => ({
      items: this.controller.formatSuggestItems(nextProps.fields, prevState.value),
    }));
  }

  public render() {
    const {isWaiting, className} = this.props;
    const {value, items, activeTooltip} = this.state;
    const suggestItems = value.lastIndexOf('=') > value.lastIndexOf(', ') ? [] : items;
    const text =
      (
        <span>
         % - любые символы. <br/>
        Поиск ИЛИ: "a;b" означает поиск a или b <br/>
        Числа: "~10;50~" означает поиск в диапазоне [-inf, 10] ИЛИ [50, +inf]
    </span>
      );
    return (
      <div ref={this.setSuggestRef} className={className}>
        <SuggestingInput
          isWaiting={isWaiting}
          placeholder="Поле_1=значение_1, Поле_N=значение_N"
          items={suggestItems}
          value={value}
          onItemClick={this.onItemClick}
          onChange={this.onChange}
          onKeyPress={this.onKeyPress}
          onClear={this.onClear}
        />
        <Tooltip
          size={TOOLTIP_SIZE.large}
          position={TOOLTIP_POSITION.bottom}
          header="Фильтрация"
          text={text}
          isActive={activeTooltip}
          onCloseClick={this.changeTooltipActive}
          onOutsideClick={this.changeTooltipActive}
        >
          <Icon onClick={this.changeTooltipActive} style={{fontSize: '30px'}} id="help"/>
        </Tooltip>
      </div>
    );
  }

  // Изменение value при изменении урла
  private onPopState = (): void => {
    this.setState({
      value: formatQueryToValue()
    });
    this.sendFilterData(false);
  }

  private onOutSideClick = (e: any): void => {
    if (!this.state.isFiltered && this.suggestingInput && !this.suggestingInput.contains(e.target) && this.state.isFocused) {
      this.sendFilterData();
      this.setState({
        isFocused: false
      });
    }
  }

  private changeTooltipActive = (): void => {
    this.setState({
      activeTooltip: !this.state.activeTooltip
    });
  }

  private setSuggestRef = (node: HTMLElement | null): void => {
    if (node) {
      const inputs = node.getElementsByTagName('input');
      this.suggestingInput = node;
      if (inputs.length) {
        this.inputField = inputs[0];
      }
    }
  }

  private sendFilterData = (isNewUrl: boolean = true): void => {
    const {onFilterChange} = this.props;
    const {value} = this.state;
    const lastEqualPosition = value.lastIndexOf('=');
    if ((lastEqualPosition + 1 < value.length || value === '') && typeof onFilterChange === 'function') {
      onFilterChange(value, isNewUrl);
      this.setState({
        isFiltered: true
      });
    }
  }

  private onKeyPress = (e: React.KeyboardEvent<HTMLElement>): void => {
    if (e.keyCode === ENTER_KEY || e.key === 'Enter') {
      if (this.inputField) {
        this.inputField.blur();
      }
      this.setFocusToEnd();
      this.sendFilterData();
    }
  }

  private onItemClick = (item): void => {
    const {begin} = CObjectsFinder.getSeparatedValue(this.state.value);
    const value: string = `${begin}${item.originTitle}=`;
    this.setState({
      value,
    }, () => {
      this.setFocusToEnd();
    });
  }

  private onChange = (value: string): void => {
    const {onFilterChange, fields} = this.props;
    this.setState({
      value,
      items: this.controller.formatSuggestItems(fields, value),
      isFiltered: false,
      isFocused: true
    });
    // обновление статуса при очистке значения
    if (typeof onFilterChange === 'function' && value === '') {
      onFilterChange(value);
      this.setState({
        isFiltered: true
      });
    }
  }

  private onClear = (): void => {
    const {onFilterChange} = this.props;
    if (typeof onFilterChange === 'function') {
      onFilterChange('');
    }
  }

  private setFocusToEnd(): void {
    if (this.inputField) {
      const valueLength: number = this.inputField.value.length;
      if (typeof this.inputField.selectionStart === 'number') {
        this.inputField.selectionStart = this.inputField.selectionEnd = valueLength;
      }
      this.inputField.focus();
    }
  }
}
