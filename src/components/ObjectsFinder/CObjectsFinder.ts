import {ISelectItem} from '@telsystems/inputs';
import {BasicController, ViewModel} from '@telsystems/common';
import {IFieldsName} from './ObjectsFinder';
import {handledJsonParse} from '../../utils/helpers';

interface ISeparatedValue {
  begin: string;
  rest: string;
}

export class CObjectsFinder<T extends ViewModel<any>> extends BasicController<T> {
  public static storeKey = 'objectsFinderStoreKey';
  public static Model = ViewModel;
  public static connectedState = [];

  public static getSeparatedValue(value?: string): ISeparatedValue {
    const newValue: string = value || '';
    const separator: string = ', ';
    const lastSeparatorBeginPosition: number = newValue.lastIndexOf(separator);
    const lastSeparatorEndPosition: number = lastSeparatorBeginPosition + separator.length;
    return {
      begin: lastSeparatorBeginPosition > -1 ? newValue.slice(0, lastSeparatorEndPosition) : '',
      rest: newValue.slice(lastSeparatorBeginPosition > -1 ? lastSeparatorEndPosition : 0),
    };
  }

  public model: T;

  constructor(props, context) {
    super(ViewModel, props, context);
  }

  public formatSuggestItems(items: IFieldsName[], value?: string): ISelectItem[] {
    const {begin, rest} = CObjectsFinder.getSeparatedValue(value);
    return items.reduce((res: ISelectItem[], item: IFieldsName) => {
      const {id, name, title} = item;
      const newTitle: string = name || title || '';
      const displayedTitle: string = `${newTitle}${id === newTitle ? '' : `/${id}`}`;
      if (
        isStringIncludesValue(displayedTitle, rest)
        && (begin.length ? !isStringIncludesValue(begin, id) : true)
      ) {
        res.push({
          content: {
            title: displayedTitle,
            originTitle: id,
          }
        });
      }
      return res;
    }, []);
  }
}

export function formatQueryToValue(): string {
  return decodeURI(window.location.search.substring(1));
}

export function formatValueToQuery(value: string): string {
  // %25 - encodeURI('%')
  return `?${value.replace(/, /g, '&').replace(/%/g, '%25')}`;
}

function isStringIncludesValue(stringProp: string, value: string): boolean {
  return new RegExp(escapeRegExp(value), 'i').test(stringProp);
}

function escapeRegExp(value: string): string {
  return (value).replace(/[.?+*^$[\]\\(){}|-]/g, '\\$&');
}
