import {TRequestHelper} from '../declarations/main';
import {Record} from '@telsystems/helpers';
import {api} from '@telsystems/common';

export function getFilteredDataObjectTable(request: TRequestHelper, object: string, filter: string = '', params: any = {}): Promise<{ data: any }> {
  const filterObj = filter && filter.includes('=')
    ? filter.split(',').reduce((res, v) => {
      const pos = v.indexOf('=');
      if (pos !== -1) {
        const fieldName = v.substring(0, pos).trim();
        res[fieldName] = v.substring(pos + 1).trim();
      }
      return res;
    }, {})
    : {};
  const record = new Record(filterObj);
  const requestParams = Object.assign({}, params);
  if (record.data && Object.keys(record.data).length) {
    requestParams.filter = JSON.stringify(record.data);
  }
  return request(api(getObjectUrl(object))).get({
    params: requestParams
  });
}

function getObjectUrl(object: string): string {
  return `admin/v1/${object}/read`;
}
