import * as moment from 'moment';
import {DATE_FORMAT, isMomentValid} from '../../utils/helpers';
import {utcToTimezone} from '@telsystems/helpers';
import {ISchemeItem, ITableRecord} from '@telsystems/table';
import {FailedCalls, IFilterItemType, IFiltersField, SuccessCalls} from '../../components/Filters';
import {PATHS} from '../../routes/paths';

export interface ITableData {
  scheme: ISchemeItem[];
  records: ITableRecord[];
}

export interface ICommonJournalData {
  columns: string[];
  site?: string;
}

export interface IJournalRow {
  [key: string]: any;
}

export interface IJournalData extends ICommonJournalData {
  rows: IJournalRow[];
}

export class Journal {
  public static getUrl(page: string, fields): string {
    return `${PATHS.INDEX}/${page}?${fields
      .filter(({value}) => value)
      .map(filterFieldToUrlString)
      .join('&')}`;
  }

  public static getValuesFromUrlSearch(page: string) {
    const urlFields = getUrlParams(location.search);
    return getFieldsByPage(page).map(field => ({
      ...field,
      value: urlFields[field.id]
        ? stringToInterface(urlFields[field.id], field.type)
        : field.value && filterValueToInterface(field.value, field.type)
    }));
  }

  public static getFilterFields(page: string) {
    const activeFields = getUrlParams(location.search);
    return getFieldsByPage(page).map(
      field =>
        activeFields[field.id]
          ? {
            ...field,
            value: stringToFieldValue(activeFields[field.id], field.type)
          }
          : field
    );
  }

  public data: IJournalData;

  constructor(rawData: IJournalData[]) {
    this.data = getCommonData(rawData);
  }

  public getTableData(): ITableData {
    const {columns, rows} = this.data;
    const newRows = rows.map((row) => {
      const keysWithTime = Object.keys(row).filter((key) => {
        const keyToCheck = key.toLowerCase();
        return keyToCheck.includes('time') || keyToCheck.includes('experesdt');
      });
      return keysWithTime.reduce((res: IJournalRow, key) => {
        const value = row[key];
        if (isMomentValid(value)) {
          res[key] = utcToTimezone(value, DATE_FORMAT);
        }
        return res;
      }, Object.assign({}, row));
    });
    const scheme = columns.map(c => ({
      id: c,
      title: c,
      width: 150
    }));
    return {scheme, records: newRows};
  }
}

function addColumnNamesToRows(columns: string[], rows: IJournalRow[], site: string): IJournalRow[] {
  return rows.map((row) => {
    return row.reduce((res, item, index) => {
      return Object.assign(res, {[columns[index]]: item}, {['storagesite']: site});
    }, {});
  });
}

function getCommonData(data: IJournalData[]): IJournalData {
  const initialData: IJournalData = {
    columns: data.length ? [...data[0].columns, 'storagesite'] : [],
    rows: [],
  };
  return data.reduce((res, item) => {
    const {columns, rows, site} = item;
    const rowsWithNames: IJournalRow[] = addColumnNamesToRows(columns, rows, site ? site.toString() : '');
    return Object.assign(res, {
      rows: [...res.rows, ...rowsWithNames],
    });
  }, initialData);
}

function stringToFieldValue(value, type) {
  switch (type) {
    case IFilterItemType.TEXT_LIST:
      return value.split(';');
    case IFilterItemType.NUMBER_RANGE:
      const [numberFrom, numberTo] = value
        .split(';')
        .map(str => parseInt(str, 10));
      return {from: numberFrom, to: numberTo};
    case IFilterItemType.DATE_RANGE:
      const [from, to] = value.split(';');
      const {date: dateFrom, time: timeFrom} = stringToFilterDate(from);
      const {date: dateTo, time: timeTo} = stringToFilterDate(to);
      return {dateFrom, timeFrom, dateTo, timeTo};
    default:
      return value;
  }
}

function stringToFilterDate(str: string) {
  const date = moment(str);
  return {
    date,
    time: {
      hours: date.hours(),
      minutes: date.minutes()
    }
  };
}

function getUrlParams(search: string) {
  const hashes = search.slice(search.indexOf('?') + 1).split('&');
  const params = {};
  hashes.map(hash => {
    const [key, val] = hash.split('=');
    params[key] = decodeURIComponent(val);
  });

  return params;
}

function filterFieldToUrlString(field: IFiltersField): string {
  const {id, value, type} = field;

  return `${id}=${filterValueToString(value, type)}`;
}

function filterValueToString(value, type) {
  switch (type) {
    case IFilterItemType.TEXT_LIST:
      return value.join(';');
    case IFilterItemType.NUMBER_RANGE:
      return `${value.from};${value.to}`;
    case IFilterItemType.DATE_RANGE:
      const {dateFrom, dateTo, timeFrom, timeTo} = value;
      return `${filterDateToString(dateFrom, timeFrom)};${filterDateToString(
        dateTo,
        timeTo
      )}`;
    default:
      return value;
  }
}

function filterDateToString(
  date: any,
  time: { hours: number; minutes: number }
) {
  const {hours, minutes} = time;
  return `${date.format('YYYY-MM-DD')} ${hours
    .toString()
    .padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:00`;
}

function stringToInterface(value, type) {
  switch (type) {
    case IFilterItemType.TEXT_LIST:
      return value.split(';');
    case IFilterItemType.NUMBER_RANGE:
      const [numberFrom, numberTo] = value
        .split(';')
        .map(str => parseInt(str, 10));
      return {from: numberFrom, to: numberTo};
    case IFilterItemType.DATE_RANGE:
      const [dateFrom, dateTo] = value.split(';');
      return {from: dateFrom, to: dateTo};
    default:
      return value;
  }
}

function filterValueToInterface(value, type) {
  switch (type) {
    case IFilterItemType.DATE_RANGE:
      const {dateFrom, dateTo, timeFrom, timeTo} = value;
      return {
        from: `${filterDateToString(dateFrom, timeFrom)}`,
        to: `${filterDateToString(dateTo, timeTo)}`
      };
    default:
      return value;
  }
}

function getFieldsByPage(page: string): IFiltersField[] {
  switch (page) {
    case PATHS.ERROR_CALLS:
      return FailedCalls;
    case PATHS.SUCCESS_CALLS:
      return SuccessCalls;
    default:
      return [];
  }
}
