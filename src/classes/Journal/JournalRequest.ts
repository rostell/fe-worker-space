import {timezoneToUtc} from '@telsystems/helpers';
import {IFilterItemType, IFiltersField} from '../../components/Filters';
import {DATE_FORMAT} from '../../utils/helpers';

type TSqlCondition = string | null;

export interface IJournalParams {
  // Фильтр по значению полей. Задается в виде JSON. Пример: filter={"name": "Вася"}
  filters?: any;
  // Список полей для вывода. Пример: select=id,name
  select?: string;
  from?: number;
  count?: number | string;
}

export class JournalRequestManager {
  public static baseUrl = '/api/stat/v2/journal';

  public static successCalls(params: IJournalParams) {
    const {from, count, filters = [], ...restParams} = params;
    return (request): Promise<any> => {
      return request(`${this.baseUrl}/read`)
        .get({
          params: {
            ...restParams,
            sqltext: this.getSqlQuery(successCallsSQL(filters), from, count)
          }
        })
        .then(onLoadQuery);
    };
  }

  public static errorCalls(params: IJournalParams) {
    const {from, count, filters = [], ...restParams} = params;
    return (request): Promise<any> => {
      return request(`${this.baseUrl}/read`)
        .get({
          params: {
            ...restParams,
            sqltext: this.getSqlQuery(errorCallsSQL(filters), from, count)
          }
        })
        .then(onLoadQuery);
    };
  }

  private static getSqlQuery(
    sql: string,
    from: number = 0,
    count: number | string = 'ALL'
  ): string {
    return `${sql.replace(/\r|\n/g, ' ')} LIMIT ${count} OFFSET ${from};`;
  }
}

export function fieldToSqlCondition(field: IFiltersField): TSqlCondition {
  const {value, type} = field;
  if (!value) {
    return null;
  }

  switch (type) {
    case IFilterItemType.NUMBER:
    case IFilterItemType.TEXT_EQUAL:
      return `${field.sqlId} = '${field.value}'`;
    case IFilterItemType.TEXT:
      return `${field.sqlId} ilike '%${field.value}%'`;
    case IFilterItemType.TEXT_LIST:
      const condArr = field.value.map(str => `${field.sqlId} ilike '%${str}%'`);
      return condArr.length ? `(${condArr.join(' or ')})` : null;
    case IFilterItemType.NUMBER_RANGE:
      return `${field.sqlId} >= ${field.value.from} and ${field.sqlId} <= ${field.value.to}`;
    case IFilterItemType.DATE_RANGE:
      return `${field.sqlId} >= '${field.value.from}' and ${field.sqlId} <= '${field.value.to}'`;
    default:
      return null;
  }
}

export function getFiltersQuery(filters): TSqlCondition {
  const newFilters = filters.map((filter) => {
    if (filter.type === IFilterItemType.DATE_RANGE) {
      filter.value = filter.value && filter.value.from && filter.value.to
        ? {from: timezoneToUtc(filter.value.from, DATE_FORMAT), to: timezoneToUtc(filter.value.to, DATE_FORMAT)}
        : filter.value;
    }
    return filter;
  });
  const filterFields = newFilters.reduce((acc, field) => {
    const condition = fieldToSqlCondition(field);
    return condition ? [...acc, condition] : acc;
  }, []);
  return filterFields.length ? filterFields.join(' and ') : null;
}

const onLoadQuery = (data) => {
  if (data && data.resultcode === 0) {
    return data;
  }
  throw new Error(
    `Ошибка сервера.${data ? `\ncode: ${data.resultcode}\nmsg: ${data.resultmsg}` : ''}`
  );
};

function successCallsSQL(filters): string {
  const filtersQuery: TSqlCondition = getFiltersQuery(filters);
  return `
  SELECT
    inviteid as ID,
    invitedt as DTime,
    anumber as DialFromNumber,
    adomain as DialFromTD,
    adisplayname as ConnectFromDN,
    acallednum as DialToNumber,
    arepresentative as ConnectFromNumber,
    case when bouter is true then 'Внешняя линия' when bouter is false then 'Внутренняя линия' end as FromDirection,
    aprovidercode as FromSIPoperatorCode,
    bnumber as ConnectToNumber,
    bdomain as ConnectToTD,
    bdisplayname as ConnectToDN,
    bnetworkaddr as ConnectToIPPort,
    case when bouter is true then 'Внешняя линия' when bouter is false then 'Внутренняя линия' end as ToDirection,
    bprovidercode as ToSIPOperatorCode,
    dial_dt as TimeDial,
    start_dt as TimePickUp,
    stop_dt as TimeStop,
    duration as DurationNumber,
    stopreason as StopReason,
    isrec as IsRec,
    callrecpath as RecPath_in_Storage,
    callstoragecode as StorageCode,
    callrecsize as RecSize,
    recexpirets as ExperesDT,
    callrecdel_dt as RecDeleteDT
  FROM
    calls.success
  ${filtersQuery ? `WHERE ${filtersQuery}` : ''}
  ORDER BY
    invitedt desc
   `;
}

function errorCallsSQL(filters): string {
  const filtersQuery: TSqlCondition = getFiltersQuery(filters);
  return `
  SELECT
    inviteid as ID,
    invitedt as Time,
    fromnumber as DialFromNumber,
    fromdomain as DialFromTD,
    case when fromouter ilike 'true' then 'Внешняя линия' when fromouter ilike 'false' then 'Внутренняя линия' end as FromDirection,
    fromprovidercode as FromSIPoperatorCode,
    callednum as DialToNumber,
    networkaddr as DialFromIP,
    sipcode as SipCode,
    reason as Reason
  FROM
    calls.failed
  ${filtersQuery ? `WHERE ${filtersQuery}` : ''}
  ORDER BY
    invitedt desc
  `;
}
