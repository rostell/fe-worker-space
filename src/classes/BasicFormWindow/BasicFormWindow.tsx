import * as React from 'react';
import cn from 'classnames';
import {Layout, Header, Footer, Sider, Content} from '@telsystems/design';
import {Dialog} from '@telsystems/modals';
import style from './FormWindow.scss';

export interface IBasicFormWindowProps {
  title: string;
  onClose: () => void;
  footerElements?: JSX.Element[] | JSX.Element;
  isWaiting?: boolean;
  error?: string | null;
  show?: boolean;
  shouldFormClose?: () => boolean;
  className?: string;
  contentClassname?: string;
}

export class BasicFormWindow<P extends IBasicFormWindowProps, S> extends React.Component<P, S> {
  public componentWillReceiveProps(nextProps: IBasicFormWindowProps) {
  }

  public render() {
    const {footerElements, onClose, show, shouldFormClose, error, className, contentClassname} = this.props;
    const sidebarContent = this.renderSidebar();
    return (
      <Dialog
        show={show}
        modal={true}
        closable={true}
        onCloseClick={onClose}
        hideOnEscapeClick={true}
        hideOnOverlayClick={false}
        shouldComponentClose={shouldFormClose}
      >
        <Layout className={cn(style['form'], className)}>
          <Header className={cn(style['header'])}>
            {this.renderFormTitle()}
            {this.renderHeaderContent()}
          </Header>
          <Content className={style['content-box']}>
            <Layout>
              <Sider className={cn(style['sidebar'], sidebarContent === null ? style['empty'] : '')}>
                {sidebarContent}
              </Sider>
              <Content
                className={cn(style['content'], sidebarContent !== null ? style['with-sidebar'] : '', contentClassname)}
              >
                {this.renderFormComponent()}
              </Content>
            </Layout>
          </Content>
          {(footerElements || error) && <Footer className={style['footer']}>
            {error && <div className={style['error']}>{error}</div>}
            {footerElements}
          </Footer>}
        </Layout>
      </Dialog>
    );
  }

  protected renderFormTitle = (): JSX.Element => {
    return <div className={style['header-title']}>{this.props.title}</div>;
  }

  protected renderFormComponent = (): JSX.Element | null => {
    return null;
  }

  protected renderSidebar = (): JSX.Element | JSX.Element[] | null => {
    return null;
  }

  protected renderHeaderContent = (): JSX.Element | JSX.Element[] | null => {
    return null;
  }
}
