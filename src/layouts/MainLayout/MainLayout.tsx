import * as React from 'react';
import {connect} from 'react-redux';
import cn from 'classnames';
import roleAppConf from '../../../roleapp.json';
import {Layout, Footer, Content, Row, Col, Icon} from '@telsystems/design';
import {IInfoState, IUserState, loadInfo, AppLayout, Header, ILocation} from '@telsystems/common';
import {Sidebar} from './Sidebar';
import style from './MainLayout.scss';

interface IInternalProps {
  info: IInfoState;
  user: IUserState;
  loadInfo: () => Promise<any>;
}

interface IConnectedProps {
  location: ILocation;
  match: any;
  history: any;
}

type TMergedProps = IInternalProps & IConnectedProps;

interface IState {
  showPlaceholder: boolean;
}

@connect(state => state, {loadInfo})
export default class MainLayout extends React.Component <TMergedProps, IState> {
  constructor(props: TMergedProps) {
    super(props);
    const {loadInfo} = props;
    loadInfo().then((resp) => {
      this.setState({showPlaceholder: false});
    });
    this.state = {
      showPlaceholder: false,
    };
  }

  public componentDidMount() {
    this.setState({showPlaceholder: true});
  }

  public componentWillReceiveProps(nextProps) {
  }

  public render() {
    const {
      children,
      info,
      history
    } = this.props;
    const {page} = this.props.match.params;
    const {showPlaceholder} = this.state;
    const loadedClass: string = showPlaceholder ? '' : style['loaded'];
    return (
      <AppLayout>
        <Layout
          className={cn(style['main-layout'], loadedClass)}
        >
          <Sidebar
            activeItem={page}
            className={style['sidebar']}
            showPlaceholder={showPlaceholder}
          />
          <Layout>
              <Header
                userInfo={info.data}
                logoPath={'/api/resource/v1/logo/get'}
                title={roleAppConf ? roleAppConf.title : 'Worker space'}
              />
            <Content className={style['page-content']}>
              <Row className={style['page-row']}>
                <Col span={42} offset={3}>
                  {children}
                </Col>
              </Row>
            </Content>
            <Footer className={style['footer']}/>
          </Layout>
        </Layout>
      </AppLayout>
    );
  }
}
