import * as React from 'react';
import cn from 'classnames';
import {Link} from 'react-router-dom';
import Scrollbars from 'react-custom-scrollbars';
import style from './Sidebar.scss';
import {ISidebarItem, sidebarItems} from './sidebarItems';
import {Icon} from '@telsystems/design';
import {PATHS} from '../../routes/paths';

interface IProps {
  showPlaceholder?: boolean;
  className?: string;
  activeItem: string;
}

export class Sidebar extends React.PureComponent <IProps> {
  public render() {
    const {showPlaceholder = true, className, activeItem} = this.props;
    const loadedClass: string = showPlaceholder ? '' : style['loaded'];
    const placeholderItems: JSX.Element[] = [];
    if (showPlaceholder) {
      for (let i = 0; i < 10; i++) {
        placeholderItems.push(<li className={cn(style['item'], style['placeholder'])} key={i}/>);
      }
    }
    return (
      <div className={cn(style['sidebar'], loadedClass, className)}>
        <Scrollbars autoHide={true}>
          <div className={style['sidebar-box']}>
            {
              showPlaceholder
                ? placeholderItems
                : sidebarItems.map((item: ISidebarItem, index) => {
                  const isActive = !activeItem && index === 0 ? true : item.url && activeItem === item.url;
                  return (
                    <div
                      className={cn(style['sidebar-items'], style['item'], isActive ? style['active'] : '')}
                      key={index}
                    >
                      {
                        item.nested && item.nested.length
                          ? <>
                            <Icon className={style['group-icon']} id={'breadcrumbs'}/><span>{item.title}</span>
                            {
                              item.nested.map(({title, url}, index) => {
                                const isActive = activeItem === url;
                                return (
                                  <div key={index} className={cn(style['item-nested'], isActive ? style['active'] : '')}>
                                    <Link to={`${PATHS.INDEX}/${url}`}>{title}</Link>
                                  </div>
                                );
                              })}
                          </>
                          : <Link to={`${PATHS.INDEX}/${item.url}`}>
                            {item.title}
                          </Link>
                      }
                    </div>
                  );
                })
            }
          </div>
        </Scrollbars>
      </div>
    );
  }
}
