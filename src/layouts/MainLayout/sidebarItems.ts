import {PATHS} from '../../routes/paths';

export interface ISidebarItem {
  nested?: ISidebarItem[];
  url?: string;
  title: string;
}

export const sidebarItems: ISidebarItem[] = [
  {
    title: 'Адресная книга',
    url: PATHS.ADDRESS_BOOK
  },
  {
    title: 'Журнал звонков',
    nested: [
      {title: 'Успешные звонки', url: PATHS.SUCCESS_CALLS},
      {title: 'Ошибочные звонки', url: PATHS.ERROR_CALLS},
    ]
  },
  {
    title: 'Правила переадресации',
    url: PATHS.FORWARDING
  },
];
