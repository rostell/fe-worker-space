import {IStateBasePage, MBasePage} from '../../components/BasePageClass';
import {IResponse} from '../../declarations/main';

export interface IAccItem {
  expires: number;
  state: string;
}

export interface IStatusAccount {
  username: string;
  items: IAccItem[];
}

export interface IUserStatus {
  type: string;
  accounts: IStatusAccount[];
}

export interface IStateAddressBook extends IStateBasePage {
  statuses?: IUserStatus[];
}

export default class MAddressBook extends MBasePage {
  public getStatuses(): IUserStatus[] {
    return this.state['statuses'] || [];
  }

  public onStatusesLoadSuccess(resp: IResponse): IStateAddressBook {
    if (resp.resultcode !== 0) {
      return this.onFail(resp.resultmsg);
    }
    return this.updateState({isWaiting: false, error: null, statuses: resp.data ? resp.data.accountGroups : []});
  }
}
