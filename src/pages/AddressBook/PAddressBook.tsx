import * as React from 'react';
import * as MVC from 'react-redux-mvc';
import cn from 'classnames';
import style from './AddressBook.scss';
import {RouteComponentProps} from 'react-router';
import MAddressBook, {IAccItem} from './MAddressBook';
import {CAddressBook} from './CAddressBook';
import {IDataSchemeItem, PBasePage} from '../../components';
import {Toolbar} from './Toolbar';

export interface ICreateAddressBookPage extends RouteComponentProps<any> {
}

export interface IAddressBookState {
}

@MVC.withController(CAddressBook)
export class PAddressBook extends PBasePage {
  public controller: CAddressBook<MAddressBook>;
  public formTitle = 'контакта';

  protected renderToolbar = (): JSX.Element => {
    return (
      <Toolbar
        onFilterChange={this.controller.onFilterChange}
        fields={this.controller.getFieldNames()}
      />
    );
  }

  protected renderCustomField = (data: IDataSchemeItem): JSX.Element => {
    const formData = this.controller.getFormData();
    const statuses = this.controller.getStatuses();
    const registeredStatuses = statuses.reduce((res: IAccItem[], item) => {
      item.accounts.forEach((acc) => {
        if (formData && (acc.username === formData.numlocal || acc.username === formData.numpublic)) {
          res.push(...acc.items);
        }
      });
      return res;
    }, []);
    return (
      <div>
        {
          registeredStatuses.length
            ? <div className={cn(style['status'], style['status-registered'])}>Зарегистрирован</div>
            : <div className={cn(style['status'], style['status-unregistered'])}>Незарегистрирован</div>
        }
      </div>
    );
  }
}
