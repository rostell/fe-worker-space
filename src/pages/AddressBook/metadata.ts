import {EFieldType, IDataSchemeItem} from '../../components';

// схема на которой строится таблица и форма
export const addressBookTableScheme: IDataSchemeItem[] = [
  {id: 'statuses', title: '', width: 150, field: EFieldType.CUSTOM, separatorTitle: 'Статус'},
  {id: 'id', title: 'id', width: 150, separatorTitle: 'Основные', disabled: true},
  {id: 'displayname', title: 'Имя абонента', width: 150},
  {id: 'number', title: 'Основной номер', width: 150, separatorTitle: 'Номера телефонов'},
  {id: 'mobile', title: 'Мобильный номер', width: 150},
  {id: 'mobile2', title: 'Мобильный номер 2', width: 150},
  {id: 'mobile3', title: 'Мобильный номер 3', width: 150},
  {id: 'numlocal', title: 'Локальный внутренний номер', width: 150},
  {id: 'numpublic', title: 'Публичный внутренний номер', width: 150},
  {id: 'jobtitle', title: 'Должность', width: 150, separatorTitle: 'Должность'},
  {id: 'department', title: 'Отдел', width: 150},
  {id: 'roomnum', title: 'Номер офиса/комнаты', width: 150},
  {id: 'roomnum2', title: 'Номер офиса/комнаты 2', width: 150},
  {id: 'roomnum3', title: 'Номер офиса/комнаты 3', width: 150},
  {id: 'email', title: 'E-Mail адрес', width: 150, separatorTitle: 'E-mail'},
  {id: 'email2', title: 'E-Mail адрес 2', width: 150},
  {id: 'email3', title: 'E-Mail адрес 3', width: 150},
  {id: 'ext.comment', title: 'Комментарий', width: 150, field: EFieldType.TEXTAREA, separatorTitle: 'Комментарий'},
];
