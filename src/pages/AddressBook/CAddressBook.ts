import {loadData, loadStatuses, storeKey, updateState} from './AAdressBook';
import ViewModel, {IUserStatus} from './MAddressBook';
import {CBasePage, MBasePage} from '../../components/BasePageClass';
import {addressBookTableScheme} from './metadata';
import {IEventResult, ITableRecord} from '@telsystems/table';
import {MODAL_ANIMATION_DURATION} from '@telsystems/modals';
import {PATHS} from '../../routes/paths';
import {IFieldsName, TOnFilterChangeCallback} from '../../components/ObjectsFinder/ObjectsFinder';
import {historyPushState} from '@telsystems/helpers';
import {formatQueryToValue, formatValueToQuery} from '../../components/ObjectsFinder/CObjectsFinder';
import MAddressBook from './MAddressBook';

export class CAddressBook<T extends MAddressBook> extends CBasePage<T> {
  public static storeKey = storeKey;
  public static connectedState = [storeKey];
  public model: T;

  public pagePath = PATHS.ADDRESS_BOOK;

  public constructor(props, context) {
    super(ViewModel, props, context);
  }

  public onTableDoubleClick = (event: IEventResult<ITableRecord>): void => {
    const {record} = event;
    if (record) {
      const formData = this.model.getRecords().find(r => r.id === record.id);
      if (formData) {
        this.action(updateState, {formData});
      }
    }
  }

  public getStatuses = (): IUserStatus[] => {
    return this.model.getStatuses();
  }

  public resetFormData = (): void => {
    setTimeout(() => {
      this.action(updateState, {formData: null});
    }, MODAL_ANIMATION_DURATION);
  }

  public init(): void {
    this.action(updateState, {scheme: addressBookTableScheme, isReadOnly: true});
    this.action(loadData, {from: 0, count: null});
    this.action(loadStatuses);
    this.loadRecords(formatQueryToValue());
  }

  public onFilterChange: TOnFilterChangeCallback = (value: string, isNewUrl: boolean = true): void => {
    this.loadRecords(value);
    if (isNewUrl) {
      const query = value ? formatValueToQuery(value) : location.pathname;
      historyPushState(query);
    }
  }

  public loadRecords(filters?: string): void {
    const params = {
      from: (this.getActivePage() - 1) * this.RECORDS_BY_PAGE,
      count: this.RECORDS_BY_PAGE
    };
    this.action(loadData, params, filters);
  }

  public getFieldNames(): IFieldsName[] {
    if (!this.model.getRecords().length) {
      return [];
    }
    return this.getScheme().reduce((res: IFieldsName[], item) => {
      if ((item.name || item.title) && item.id) {
        res.push({name: item.name || item.title, id: item.id});
      }
      return res;
    }, []);
  }
}
