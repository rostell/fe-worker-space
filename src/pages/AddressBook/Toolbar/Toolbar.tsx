import * as React from 'react';
import cn from 'classnames';
import style from './Toolbar.scss';
import {Align, Left} from '@telsystems/design';
import {IFieldsName, ObjectsFinder, TOnFilterChangeCallback} from '../../../components/ObjectsFinder/ObjectsFinder';

interface IToolbarProps {
  fields: IFieldsName[];
  onFilterChange: TOnFilterChangeCallback;
}

interface IState {
}

export class Toolbar extends React.Component <IToolbarProps, IState> {
  public render() {
    const {fields, onFilterChange} = this.props;
    return (
      <div className={style['toolbar']}>
        <Align>
          <Left>
            <ObjectsFinder
              onFilterChange={onFilterChange}
              fields={fields}
              className={style['toolbar-search']}
            />
          </Left>
        </Align>
      </div>
    );
  }
}
