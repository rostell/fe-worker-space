import {createActions} from 'easy-redux';
import MAddressBook, {IStateAddressBook} from './MAddressBook';
import {api} from '@telsystems/common';
import {getFilteredDataObjectTable} from '../../redux';

export const storeKey = 'addressBook';
const UPDATE = `${storeKey}@@UPDATE`;
const RESET = `${storeKey}@@RESET`;
const LOAD_DATA = `${storeKey}@@LOAD_DATA`;
const LOAD_STATUSES = `${storeKey}@@LOAD_STATUSES`;

const initialState: IStateAddressBook = Object.assign({}, MAddressBook.defaults);

const actions = createActions({
  storeKey,
  initialState,
  actions: {
    [UPDATE]: {
      action: (updates) => ({updates}),
      handler: (state: IStateAddressBook, {updates}) => new MAddressBook(state).updateState(updates)
    },
    [RESET]: {
      action: () => ({}),
      handler: (state: IStateAddressBook) => new MAddressBook(state).reset(MAddressBook.defaults).getState()
    },
    [LOAD_DATA]: {
      action: (params, filters: string) => ({
        promise: request => getFilteredDataObjectTable(request, 'addressbook', filters, params),
      }),
      handlers: {
        onWait: (state: IStateAddressBook): IStateAddressBook => new MAddressBook(state).onWaiting(),
        onFail: (state: IStateAddressBook, {error}): IStateAddressBook => new MAddressBook(state).onFail(error),
        onSuccess: (state: IStateAddressBook, {result}): IStateAddressBook =>
          new MAddressBook(state).onDataLoadSuccess(result)
      }
    },
    [LOAD_STATUSES]: {
      action: () => ({
        promise: request => request(api(`monitor/v1/states/currentload`)).get({}),
      }),
      handlers: {
        onWait: (state: IStateAddressBook): IStateAddressBook => new MAddressBook(state).onWaiting(),
        onFail: (state: IStateAddressBook, {error}): IStateAddressBook => new MAddressBook(state).onFail(error),
        onSuccess: (state: IStateAddressBook, {result}): IStateAddressBook =>
          new MAddressBook(state).onStatusesLoadSuccess(result)
      }
    },
  }
});

export const updateState = actions[UPDATE];
export const resetModel = actions[RESET];
export const loadData = actions[LOAD_DATA];
export const loadStatuses = actions[LOAD_STATUSES];
