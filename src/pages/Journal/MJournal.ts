import {IStateBasePage, MBasePage} from '../../components/BasePageClass';
import {Journal} from '../../classes/Journal/Journal';
import {IResponse} from '../../declarations/main';

export interface IStateJournal extends IStateBasePage {
}

export default class MJournal extends MBasePage {
  public onDataLoadSuccess(resp: IResponse): IStateBasePage {
    if (resp.resultcode !== 0) {
      return this.onFail(resp.resultmsg);
    }
    const {records, scheme} = new Journal(resp.data).getTableData();
    return this.updateState({isWaiting: false, error: null, records, scheme});
  }
}
