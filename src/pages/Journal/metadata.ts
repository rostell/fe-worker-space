import {EFieldType, IDataSchemeItem} from '../../components';

export const journalFormScheme: IDataSchemeItem[] = [
  {id: 'id', title: 'id', width: 150, separatorTitle: 'Основные', disabled: true},
  {id: 'recLink', title: 'Прослушать', width: 150, separatorTitle: 'Запись звонка', field: EFieldType.PLAYER},
  {id: 'recDownloadLink', title: 'Ссылка', width: 150, field: EFieldType.CUSTOM},
];
