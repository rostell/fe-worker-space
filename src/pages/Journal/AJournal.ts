import {createActions} from 'easy-redux';
import MJournal, {IStateJournal} from './MJournal';
import {IJournalParams, JournalRequestManager} from '../../classes/';

export const storeKey = 'journal';
const UPDATE = `${storeKey}@@UPDATE`;
const RESET = `${storeKey}@@RESET`;
const LOAD_DATA = `${storeKey}@@LOAD_DATA`;

const initialState: IStateJournal = Object.assign({}, MJournal.defaults);

const actions = createActions({
  storeKey,
  initialState,
  actions: {
    [UPDATE]: {
      action: (updates) => ({updates}),
      handler: (state: IStateJournal, {updates}) => new MJournal(state).updateState(updates)
    },
    [RESET]: {
      action: () => ({}),
      handler: (state: IStateJournal) => new MJournal(state).reset(MJournal.defaults).getState()
    },
    [LOAD_DATA]: {
      action: (params: IJournalParams, isSuccess: boolean) => ({
        promise: isSuccess ? JournalRequestManager.successCalls(params) : JournalRequestManager.errorCalls(params),
      }),
      handlers: {
        onWait: (state: IStateJournal): IStateJournal => new MJournal(state).onWaiting(),
        onFail: (state: IStateJournal, {error}): IStateJournal => new MJournal(state).onFail(error),
        onSuccess: (state: IStateJournal, {result}): IStateJournal =>
          new MJournal(state).onDataLoadSuccess(result)
      }
    },
  }
});

export const updateState = actions[UPDATE];
export const resetModel = actions[RESET];
export const loadData = actions[LOAD_DATA];
