import * as React from 'react';
import * as MVC from 'react-redux-mvc';
import style from './Journal.scss';
import {RouteComponentProps} from 'react-router';
import MJournal from './MJournal';
import {CJournal} from './CJournal';
import cn from 'classnames';
import {Filters, IDataSchemeItem, PBasePage} from '../../components';
import {Align, Right} from '@telsystems/design';
import {BTN_SECONDARY_TYPE, Button, IconButton, Textarea} from '@telsystems/inputs';

export interface IJournalPage extends RouteComponentProps<any> {
}

export interface IJournalState {
}

@MVC.withController(CJournal)
export class PJournal extends PBasePage<IJournalPage, IJournalState> {
  public controller: CJournal<MJournal>;
  public formTitle = 'звонка';

  public componentDidMount() {
    this.controller.init();
  }

  protected renderToolbar = (): JSX.Element => {
    return (
      <Filters
        onChangeValue={this.controller.onChangeFilter}
        fields={this.controller.getFilterFields()}
      />
    );
  }

  protected renderCustomField = (data: IDataSchemeItem): JSX.Element => {
    if (data.id === 'recDownloadLink') {
      const recLink = this.controller.getRecLink();
      const fullLink = location.origin + recLink;
      return recLink
        ? (
          <div className={style['fullwidth']}>
            <Textarea autosize={{minRows: 5, maxRows: 5}} fullWidth={true} value={fullLink}/>
            <div className={cn(style['fullwidth'], style['link-container'])}>
              <IconButton
                className={style['link-button']}
                type={BTN_SECONDARY_TYPE}
                iconId={'download'}
                onClick={this.controller.onDownloadClick}
              >
                Скачать
              </IconButton>
              <IconButton
                className={style['link-button']}
                type={BTN_SECONDARY_TYPE}
                iconId={'copy'}
                onClick={this.controller.onCopyClick}
              >
                Копировать
              </IconButton>
            </div>
          </div>
        )
        : <div className={style['error']}>Ссылка не найдена</div>;
    }
    return <div/>;
  }
}
