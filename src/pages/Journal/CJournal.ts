import {loadData, storeKey, updateState} from './AJournal';
import ViewModel from './MJournal';
import {CBasePage, MBasePage} from '../../components/BasePageClass';
import {Journal} from '../../classes/Journal/Journal';
import {IFiltersField} from '../../components/Filters';
import {PATHS} from '../../routes/paths';
import {IEventResult, ITableRecord} from '@telsystems/table';
import {MODAL_ANIMATION_DURATION} from '@telsystems/modals';
import {timezoneToUtc} from '@telsystems/helpers';
import {journalFormScheme} from './metadata';

export class CJournal<T extends MBasePage> extends CBasePage<T> {
  public static storeKey = storeKey;
  public static connectedState = [storeKey];
  public model: T;

  public constructor(props, context) {
    super(ViewModel, props, context);
  }

  public componentWillReceiveProps(currentProps, nextProps) {
    super.componentWillReceiveProps(currentProps, nextProps);
    // is page changed
    // TODO пофиксить
    if (currentProps.location.pathname.split('/')[2] !== nextProps.location.pathname.split('/')[2]) {
      this.nextProps.history.push(`${PATHS.INDEX}/${this.getPageName()}`);
      this.init();
    }
  }

  public getPageName = (): string => {
    return this.nextProps.match.params.journal;
  }

  public init(): void {
    this.loadRecords();
  }

  public onTableDoubleClick = (event: IEventResult<ITableRecord>): void => {
    const {record} = event;
    if (record && this.getPageName() === PATHS.SUCCESS_CALLS) {
      const formData = this.model.getRecords().find(r => r.id === record.id);
      if (formData) {
        this.action(updateState, {formData: this.formatFormData(formData), formScheme: journalFormScheme});
      }
    }
  }

  public getRecLink = (id?: string): string => {
    let recLink;
    const formData = this.getFormData();
    if (id || formData) {
      const data = this.model.getRecords().find(r => (r.id === id) || !!(formData && r.id === formData.id));
      if (data && data.id && data.dtime && data.storagesite) {
        const invitedt = timezoneToUtc(data.dtime.toString(), 'YYYY-MM-DDTHH:mm:ss');
        recLink = `/api/stat/v2/journal/get_callrecord?site=${data.storagesite}&inviteid=${data.id}&invitedt=${invitedt}Z`;
      }
    }
    return recLink;
  }

  public formatFormData(formData): ITableRecord {
    return {
      id: formData.id,
      recLink: this.getRecLink(formData.id),
    };
  }

  public onCopyClick = (): void => {
    const recLink = this.getRecLink();
    if (recLink) {
      const fullLink = location.origin + recLink;
      const tempInput = document.createElement('input');
      document.body.appendChild(tempInput);
      tempInput.setAttribute('value', fullLink);
      tempInput.select();
      document.execCommand('copy');
      document.body.removeChild(tempInput);
    }
    else {
      this.action(updateState, {formError: 'Запись не может быть скачана'});
    }
  }

  public onDownloadClick = (): void => {
    const recLink = this.getRecLink();
    if (recLink) {
      window.open(`${location.origin}${recLink}`);
    }
    else {
      this.action(updateState, {formError: 'Запись не может быть скачана'});
    }
  }

  public resetFormData = (): void => {
    setTimeout(() => {
      this.action(updateState, {formData: null, formError: null, formScheme: null});
    }, MODAL_ANIMATION_DURATION);
  }

  public loadRecords(): void {
    const filters = this.getFilter();
    this.action(loadData, {filters}, this.getPageName() === PATHS.SUCCESS_CALLS);
  }

  public getFilterFields = () => {
    return Journal.getFilterFields(this.getPageName());
  }

  public onChangeFilter = (fields: IFiltersField[]) => {
    this.nextProps.history.push(Journal.getUrl(this.getPageName(), fields));
    this.loadRecords();
  }

  public getFilter = () => {
    return Journal.getValuesFromUrlSearch(this.getPageName());
  }
}
