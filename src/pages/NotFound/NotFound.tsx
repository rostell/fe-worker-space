import * as React from 'react';
import {Page} from '@telsystems/common';

export class NotFound extends React.Component {
  public render() {
    return (
      <Page>
        <h1>404 Запрашиваемая страница не найдена</h1>
      </Page>
    );
  }
}
