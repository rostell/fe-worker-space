import {EFieldType, IDataSchemeItem} from '../../components';

export const forwardingRules = [
  {title: 'Безусловная', value: 'absolute'},
  {title: 'Нет регистрации', value: 'unregistered'},
  {title: 'Занято', value: 'busy'},
  {title: 'Нет ответа', value: 'timeout'},
  {title: 'Отклонено', value: 'decline'},
  {title: 'Режим DND', value: 'dnd'},
  {title: 'Ошибка вызова', value: 'error'},
  {title: 'Другая причина', value: 'other'},
];

// схема на которой строится таблица и форма
export const forwardingTableScheme: IDataSchemeItem[] = [
  {
    id: 'id',
    title: 'id',
    width: 150,
    separatorTitle: 'Основные',
    disabled: true
  },
  {
    id: 'opts.title',
    title: 'Наименование переадресации',
    width: 150
  },
  {
    id: 'filter_number',
    title: 'Номер с которого переадресовать',
    width: 150,
    separatorTitle: 'Номера переадресаций',
    field: EFieldType.NUMBER_INPUT
  },
  {
    id: 'tran_number',
    title: 'Номер на который переадресовать',
    width: 150,
    field: EFieldType.NUMBER_INPUT
  },
  {
    id: 'type',
    title: 'Условие переадресации',
    width: 150,
    separatorTitle: 'Условие переадресации',
    items: forwardingRules,
    field: EFieldType.SELECT
  },
];
