import * as React from 'react';
import cn from 'classnames';
import style from './Toolbar.scss';
import {Align, Left, Right} from '@telsystems/design';
import {Button, IconButton} from '@telsystems/inputs';
import {IFieldsName, ObjectsFinder, TOnFilterChangeCallback} from '../../../components/ObjectsFinder/ObjectsFinder';

interface IToolbarProps {
  onCreateClick: () => void;
  fields: IFieldsName[];
  onFilterChange: TOnFilterChangeCallback;
}

interface IState {
}

export class Toolbar extends React.Component <IToolbarProps, IState> {
  public render() {
    const {onCreateClick, fields, onFilterChange} = this.props;
    return (
      <div className={style['toolbar']}>
        <Align>
          <Left>
            <ObjectsFinder
              onFilterChange={onFilterChange}
              fields={fields}
              className={style['toolbar-search']}
            />
          </Left>
          <Right>
            <IconButton iconId={'plus'} onClick={onCreateClick}>Новый объект</IconButton>
          </Right>
        </Align>
      </div>
    );
  }
}
