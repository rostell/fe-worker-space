import {createActions} from 'easy-redux';
import MForwarding, {IStateForwarding} from './MForwarding';
import {api} from '@telsystems/common';
import {getFilteredDataObjectTable} from '../../redux';

export const storeKey = 'forwarding';
const UPDATE = `${storeKey}@@UPDATE`;
const RESET = `${storeKey}@@RESET`;
const LOAD_DATA = `${storeKey}@@LOAD_DATA`;
const SAVE_DATA = `${storeKey}@@SAVE_DATA`;

const initialState: IStateForwarding = Object.assign({}, MForwarding.defaults);
const OBJECT_NAME = 'redirectrule';

const actions = createActions({
  storeKey,
  initialState,
  actions: {
    [UPDATE]: {
      action: (updates) => ({updates}),
      handler: (state: IStateForwarding, {updates}) => new MForwarding(state).updateState(updates)
    },
    [RESET]: {
      action: () => ({}),
      handler: (state: IStateForwarding) => new MForwarding(state).reset(MForwarding.defaults).getState()
    },
    [LOAD_DATA]: {
      action: (params, filters: string) => ({
        promise: request => getFilteredDataObjectTable(request, OBJECT_NAME, filters, params),
      }),
      handlers: {
        onWait: (state: IStateForwarding): IStateForwarding => new MForwarding(state).onWaiting(),
        onFail: (state: IStateForwarding, {error}): IStateForwarding => new MForwarding(state).onFail(error),
        onSuccess: (state: IStateForwarding, {result}): IStateForwarding =>
          new MForwarding(state).onDataLoadSuccess(result)
      }
    },
    [SAVE_DATA]: {
      action: (data, isNew: boolean) => ({
        promise: request => request(api(`admin/v1/${OBJECT_NAME}/${isNew ? 'create' : 'update'}`)).post({data: {data}}),
      }),
      handlers: {
        onWait: (state: IStateForwarding): IStateForwarding => new MForwarding(state).onFormWaiting(),
        onFail: (state: IStateForwarding, {error}): IStateForwarding => new MForwarding(state).onFormFail(error),
        onSuccess: (state: IStateForwarding, {result}): IStateForwarding =>
          new MForwarding(state).onFormSaveSuccess(result)
      }
    },
  }
});

export const updateState = actions[UPDATE];
export const resetModel = actions[RESET];
export const loadData = actions[LOAD_DATA];
export const saveData = actions[SAVE_DATA];
