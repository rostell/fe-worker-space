import {loadData, saveData, storeKey, updateState} from './AForwarding';
import ViewModel from './MForwarding';
import {CBasePage, MBasePage} from '../../components/BasePageClass';
import {forwardingTableScheme} from './metadata';
import {IEventResult, ITableRecord} from '@telsystems/table';
import {MODAL_ANIMATION_DURATION} from '@telsystems/modals';
import {historyPushState, Record} from '@telsystems/helpers';
import {PATHS} from '../../routes/paths';
import {IFieldsName, TOnFilterChangeCallback} from '../../components/ObjectsFinder/ObjectsFinder';
import {formatQueryToValue, formatValueToQuery} from '../../components/ObjectsFinder/CObjectsFinder';
import {IResponse} from '../../declarations/main';

export class CForwarding<T extends MBasePage> extends CBasePage<T> {
  public static storeKey = storeKey;
  public static connectedState = [storeKey];
  public model: T;

  public pagePath = PATHS.FORWARDING;

  public constructor(props, context) {
    super(ViewModel, props, context);
  }

  public init(): void {
    this.action(updateState, {scheme: forwardingTableScheme, isReadOnly: false});
    this.loadData();
  }

  public loadData = (): void => {
    this.action(loadData, {from: 0, count: null});
    this.loadRecords(formatQueryToValue());
  }

  public onTableDoubleClick = (event: IEventResult<ITableRecord>): void => {
    const {record} = event;
    if (record) {
      const formData = this.model.getRecords().find(r => r.id === record.id);
      if (formData) {
        this.action(updateState, {formData});
      }
    }
  }

  public onCreateClick = (): void => {
    this.action(updateState, {formData: {}});
  }

  public onFormDataChange = (field: string, value: any): void => {
    const formData = this.getFormData();
    if (formData) {
      const record = new Record(formData);
      record.set(field, value);
      this.action(updateState, {
        formData: record.data,
        formError: null
      });
    }
  }

  public resetFormData = (): void => {
    setTimeout(() => {
      this.action(updateState, {formData: null, formError: null});
    }, MODAL_ANIMATION_DURATION);
  }

  public onDeleteClick = (): void => {
  }

  public onSaveClick = (): void => {
    const formData = this.getFormData();
    if (formData) {
      this.action(saveData, formData, !formData.id).then((resp: IResponse) => {
        if (resp.resultcode === 0) {
          this.loadData();
          this.resetFormData();
        }
      });
    }
  }

  public onFilterChange: TOnFilterChangeCallback = (value: string, isNewUrl: boolean = true): void => {
    this.loadRecords(value);
    if (isNewUrl) {
      const query = value ? formatValueToQuery(value) : location.pathname;
      historyPushState(query);
    }
  }

  public loadRecords(filters?: string): void {
    const params = {
      from: (this.getActivePage() - 1) * this.RECORDS_BY_PAGE,
      count: this.RECORDS_BY_PAGE
    };
    this.action(loadData, params, filters);
  }

  public getFieldNames(): IFieldsName[] {
    if (!this.model.getRecords().length) {
      return [];
    }
    return this.getScheme().reduce((res: IFieldsName[], item) => {
      if ((item.name || item.title) && item.id) {
        res.push({name: item.name || item.title, id: item.id});
      }
      return res;
    }, []);
  }
}
