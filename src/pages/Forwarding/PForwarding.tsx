import * as React from 'react';
import * as MVC from 'react-redux-mvc';
import {RouteComponentProps} from 'react-router';
import MForwarding from './MForwarding';
import {CForwarding} from './CForwarding';
import {PBasePage} from '../../components';
import {Align, Left, Right} from '@telsystems/design';
import {BTN_SECONDARY_TYPE, Button, IconButton} from '@telsystems/inputs';
import {Toolbar} from './Toolbar';

export interface IForwardingPage extends RouteComponentProps<any> {
}

export interface IForwardingState {
}

@MVC.withController(CForwarding)
export class PForwarding extends PBasePage {
  public controller: CForwarding<MForwarding>;
  public formTitle = 'правила';

  protected getFooterElements = (): JSX.Element => {
    const isFormWaiting = this.controller.isFormWaiting();
    return (
      <Align>
        <Left>
          <IconButton
            iconId={'trash'}
            onClick={this.controller.onDeleteClick}
            isWaiting={isFormWaiting}
            disabled={isFormWaiting}
            type={BTN_SECONDARY_TYPE}
          >
            Удалить
          </IconButton>
        </Left>
        <Right>
          <Button disabled={isFormWaiting} isWaiting={isFormWaiting} onClick={this.controller.onSaveClick}>
            Сохранить
          </Button>
        </Right>
      </Align>
    );
  }

  protected renderToolbar = (): JSX.Element => {
    return (
      <Toolbar
        onFilterChange={this.controller.onFilterChange}
        fields={this.controller.getFieldNames()}
        onCreateClick={this.controller.onCreateClick}
      />
    );
  }
}
