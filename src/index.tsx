import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {syncHistoryWithStore} from 'react-router-redux';
import {BrowserRouter} from 'react-router-dom';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import {materialTheme} from '@telsystems/design';
import {ReactReduxMvc} from 'react-redux-mvc';
import {requestManager as requestHelper} from '@telsystems/helpers';
import routes from './routes';
import '@telsystems/design/dist/main.css';
import '@telsystems/inputs/dist/main.css';
import '@telsystems/modals/dist/main.css';
import '@telsystems/common/dist/main.css';
import '@telsystems/table/dist/main.css';
import '@telsystems/player/dist/main.css';

// to be sure that all reducers are applied createStore must be imported after import routes
import createBrowserHistory from 'history/createBrowserHistory';
import {createStore} from '@telsystems/common';

if (!(window as any).__initialData) {
  (window as any).__initialData = {};
}

const appStore = createStore({data: (window as any).__initialData.store || {}, requestHelper: requestHelper.requestManager});
syncHistoryWithStore(createBrowserHistory(), appStore);

ReactDOM.render(
  <Provider store={appStore} key="provider">
    <ReactReduxMvc store={appStore}>
      <MuiThemeProvider theme={createMuiTheme(materialTheme)}>
        <BrowserRouter>
          {routes(appStore)}
        </BrowserRouter>
      </MuiThemeProvider>
    </ReactReduxMvc>
  </Provider>,
  document.getElementById('root')
);

console.log(`App name: ${__PACKAGE_NAME__}`);
console.log(`App version: ${__PACKAGE_VERSION__}`);
