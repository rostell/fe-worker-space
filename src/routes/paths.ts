import {getRootScriptUrl} from '@telsystems/helpers';

export const PATHS = {
  INDEX: getRootScriptUrl() || '/worker_space',
  ADDRESS_BOOK: 'addresses',
  SUCCESS_CALLS: 'success-calls',
  ERROR_CALLS: 'error-calls',
  FORWARDING: 'forwarding'
};
