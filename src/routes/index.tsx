import * as React from 'react';
import {Route, Switch} from 'react-router';
import {MainLayout} from '../layouts';
import {PATHS} from './paths';
import {PAddressBook, PForwarding, PJournal} from '../pages';

function MainLayoutRoutes(store, props) {
  return (
    <MainLayout {...props}>
      <Switch>
        <Route exact={true} strict={false} path={`${PATHS.INDEX}`} component={PAddressBook}/>
        <Route
          exact={true}
          strict={false}
          path={`${PATHS.INDEX}/${PATHS.ADDRESS_BOOK}/(page)?/:page?`}
          component={PAddressBook}
        />
        <Route
          exact={true}
          strict={false}
          path={`${PATHS.INDEX}/${PATHS.FORWARDING}/(page)?/:page?`}
          component={PForwarding}
        />
        <Route
          exact={true}
          strict={false}
          path={`${PATHS.INDEX}/:journal/(page)?/:page?`}
          component={PJournal}
        />
      </Switch>
    </MainLayout>
  );
}

export default (store) => {
  return (
    <Switch>
      <Route
        exact={false}
        strict={false}
        path={`${PATHS.INDEX}/:page?`}
        component={MainLayoutRoutes.bind(null, store)}
      />
    </Switch>
  );
};
