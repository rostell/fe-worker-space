declare const __PACKAGE_NAME__: string;
declare const __PACKAGE_VERSION__: string;
declare const __INDEX_PATH__: string;

declare module '*.css' {
  interface IClassNames {
    [className: string]: string;
  }

  const classNames: IClassNames;
  export default classNames;
}

declare module '*.scss' {
  interface IClassNames {
    [className: string]: string;
  }

  const classNames: IClassNames;
  export default classNames;
}

declare module '*.json' {
  const content: any;
  export default content;
}

declare module '*.html' {
  const content: any;
  export default content;
}
