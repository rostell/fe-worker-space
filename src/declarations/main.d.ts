interface IInitialState {
  store: object;
}
// если используется SSR то данные, будут приходить уже вместо с отрендеренной страницей
declare const __initialData: IInitialState;

interface IRequestMethods {
  get: (args: {params: object | string}) => Promise<any>;
  post: (args: {data: object}) => Promise<any>;
}

export type TRequestHelper = (url: string) => IRequestMethods;

export interface IResponse {
  resultmsg: string;
  data: any;
  resultcode: number;
}
