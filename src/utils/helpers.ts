import * as moment from 'moment';

export const DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';

export function isMomentValid(time: string): boolean {
  return moment(time, DATE_FORMAT).isValid();
}

export function handledJsonParse(obj: any, options?): {value, error: string | null} {
  let value = null;
  let error = null;
  try {
    value = JSON.parse(obj, options);
  }
  catch (err) {
    error = err;
    console.error(`Ошибка при парсинге: `, {obj, error});
  }
  return {error, value};
}
