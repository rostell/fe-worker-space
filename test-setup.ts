import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as vendor from './node_modules/@telsystems/vendor/dist/vendor.js';

(global as any).vendor = vendor;
(global as any).__INDEX_PATH__ = undefined;

Enzyme.configure({ adapter: new Adapter() });
window.matchMedia = window.matchMedia || function() {
  return {
    matches: false,
    addListener: function() {},
    removeListener: function() {}
  };
};

window.requestAnimationFrame = window.requestAnimationFrame || function(callback) {
  setTimeout(callback, 0);
};
