const path = require('path');
const jestBaseConfig = require(path.join(__dirname,'./node_modules/@telsystems/build-configs/dist/staticFiles/jestBaseConfig.json'));
module.exports = {
  ...jestBaseConfig,
  "setupFiles": ["./test-setup.ts"]
};
