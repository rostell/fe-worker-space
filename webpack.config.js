const path = require('path');
const webpack = require('webpack');
const {AppDev, AppProd} = require('@telsystems/build-configs');
const AddAssetsPlugin = require('add-asset-html-webpack-plugin');
const appConf = require('./appConf.json');
const roleapp = require('./roleapp.json');

const {NODE_ENV} = process.env;
const port = appConf.devServerPort || undefined;
const serviceUrl = appConf.serviceUrl || 'localhost';
const modulesPath = path.resolve(__dirname, './node_modules');

if (NODE_ENV === 'development') {
  module.exports = new AppDev(webpack)
    .setDirname(__dirname)
    .setHtmlTitle(roleapp.name)
    .disableHttpsDevServer()
    .setIndexPageTemplatePath(path.resolve(__dirname, './node_modules/@telsystems/common/dist/staticFiles/index.ejs'))
    .addPlugin(
      new AddAssetsPlugin([
        {
          filepath: path.resolve(__dirname, './node_modules/@telsystems/vendor/dist/vendor.js'),
          includeSourcemap: false,
          hash: true
        }
      ])
    )
    .addPlugin(
      new webpack.DllReferencePlugin({
        manifest: require("./node_modules/@telsystems/vendor/dist/vendor-manifest.json"),
        extensions: [".js"]
      })
    )
    .setDevServerOptions({
      port,
      serviceUrl
    })
    .setDevServerContentBase(path.resolve(__dirname, 'src'))
    .setModulePaths([modulesPath])
    .create();
}
else {
  module.exports = new AppProd(webpack)
    .setDirname(__dirname)
    .setHtmlTitle(roleapp.name)
    .setIndexPageTemplatePath(path.resolve(__dirname, './node_modules/@telsystems/common/dist/staticFiles/index.ejs'))
    .setPublicPath(`./`)
    .addPlugin(
      new AddAssetsPlugin([
        {
          filepath: path.resolve(__dirname, './node_modules/@telsystems/vendor/dist/vendor.js'),
          includeSourcemap: false,
          hash: true,
          publicPath: '/assets'
        }
      ])
    )
    .addPlugin(
      new webpack.DllReferencePlugin({
        manifest: require("./node_modules/@telsystems/vendor/dist/vendor-manifest.json"),
        extensions: [".js"]
      })
    )
    .create();
}
